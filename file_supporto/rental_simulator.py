import requests
import random
import time
import json
import urllib3
from datetime import date


URL = 'https://norwegiangoat.ddns.net/bike_app/api/'
ACTION = ['rastrelliera/', 'noleggio_utente/',
          'termina_noleggio/', 'prenotazione_utente/']
# Id: 21, 22, 23, 24
USERS = ["riccardomioli@gmail.com", "benny@hotmail.com",
         "matteo@gmail.it", "miky@icloud.com"]
TOKEN = ['3485e3311dc14e34f16f122d01bc5f21fef23e4d', 'a1645bf521680d17631b1873e5c16abc63ec585e',
         'b2d37a24414513b4a7a0137ad028a7725cd4d464', 'f46e0ea9d0f615ea474cd8679dcb45ee24371240']
rastrelliere = None
request_header = None


def book_bike():
    rastrelliera = choose_starting_point(rastrelliere)
    bike_id = rastrelliera['bicicletta'][0]
    booking_day = str(date.fromtimestamp(
        time.time() + random.randint(86400, 31536000)))
    booking_data = '{"giornata_prenotata": "' + \
        booking_day + '","bicicletta":' + str(bike_id) + '}'
    print(booking_data)
    post_response = requests.post(
        url=URL + ACTION[3], data=booking_data, headers=request_header)
    print(post_response.text)
    print("Response:", post_response.status_code)


def choose_starting_point(rastrelliere):
    random.shuffle(rastrelliere)
    for rastrelliera in rastrelliere:
        if rastrelliera['bicicletta']:
            # Check that this rastrelliera has some bikes
            return rastrelliera


def post_position(longitude, latitude, bike_id):
    rental_data = '{"codice_sblocco": " ","location": {"type": "Point", "coordinates": ['+str(
        longitude)+','+str(latitude)+']}}'
    print(rental_data)
    post_response = requests.post(
        url=URL + ACTION[1] + str(bike_id) + '/', data=rental_data, headers=request_header)
    print(post_response.text)
    print("Response:", post_response.status_code)


def simulate_rental():
    # Choose one rastrelliera as start point, one as end point and a bike
    starting_point = choose_starting_point(rastrelliere)
    print("Start Point: ", starting_point)
    end_point = rastrelliere[random.randrange(0, len(rastrelliere))]
    print("End Point: ", end_point)
    longitude = starting_point['location']['coordinates'][0]
    latitude = starting_point['location']['coordinates'][1]

    # Takes the first available bike
    bike_id = starting_point['bicicletta'][0]
    print("Choosen Bike: ", bike_id)

    # Generate some random waypoints
    for i in range(0, random.randrange(10, 50)):
        latitude += random.uniform(-0.01, 0.01)
        longitude += random.uniform(-0.01, 0.01)
        post_position(longitude, latitude, bike_id)
        # Update position every 10 seconds, like the android client.
        # time.sleep(10)

    # Close the rental. Add last position near to the end_point
    longitude = end_point['location']['coordinates'][0]
    latitude = end_point['location']['coordinates'][1]
    post_position(longitude, latitude, bike_id)
    end_rental = requests.post(
        url=URL + ACTION[2] + str(bike_id) + '/', data='{}', headers=request_header)
    print(end_rental.text)
    print("Response:", end_rental.status_code)


if __name__ == "__main__":
    # Disable subjectAltName warning
    urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)

    # Get rastrelliere using a fixed user
    request_header = {'Authorization': 'Token ' + TOKEN[0]}
    rastrelliere = requests.get(
        url=URL + ACTION[0], data='{}', headers=request_header).json()

    # Generate 30 rentals
    for i in range(0, 30):
        # Choose random user from user list
        user = random.randrange(0, len(USERS))
        print('Chosen user:', USERS[user])
        request_header = {'Authorization': 'Token ' + TOKEN[user]}
        # User books a bike
        book_bike()
        # User start a rental
        simulate_rental()
