import requests
import random
import time
import json
import urllib3
import numpy


URL = 'https://norwegiangoat.ddns.net/bike_app/api/'
ACTION = ['rastrelliera/', 'noleggio_utente/', 'termina_noleggio/']
USERS = "riccardomioli@gmail.com"
TOKEN = '3485e3311dc14e34f16f122d01bc5f21fef23e4d'
ROUNDS = 30
latency = []
geofences = None


def choose_starting_point(rastrelliere):
    for rastrelliera in rastrelliere:
        if rastrelliera['bicicletta']:
            # Check that this rastrelliera has some bikes
            return rastrelliera


def post_position(longitude, latitude, bike_id, request_header):
    rental_data = '{"codice_sblocco": " ","location": {"type": "Point", "coordinates": ['+str(
        longitude)+','+str(latitude)+']}}'
    print(rental_data)
    # Time now
    t0 = time.time()
    post_response = requests.post(
        url=URL + ACTION[1] + str(bike_id) + '/', data=rental_data, headers=request_header)
    # Time after response
    t1 = time.time()
    print(post_response.text)
    latency.append(t1 - t0)
    print("Response:", post_response.status_code)


def simulate_rental():
    # Choose one rastrelliera as start point, one as end point and a bike
    request_header = {'Authorization': 'Token ' + TOKEN}
    rastrelliere = requests.get(
        url=URL + ACTION[0], data='{}', headers=request_header).json()
    starting_point = choose_starting_point(rastrelliere)
    print("Start Point: ", starting_point)
    end_point = rastrelliere[random.randrange(0, len(rastrelliere))]
    print("End Point: ", end_point)
    longitude = starting_point['location']['coordinates'][0]
    latitude = starting_point['location']['coordinates'][1]

    # Takes the first available bike
    bike_id = starting_point['bicicletta'][0]
    print("Choosen Bike: ", bike_id)

    # Generate some random waypoints
    for geofence in geofences:
        longitude = geofence["geometry"]['coordinates'][0][0][0]
        latitude = geofence["geometry"]['coordinates'][0][0][1]
        post_position(longitude, latitude, bike_id, request_header)
        # Update position every 10 seconds, like the android client.
        # time.sleep(10)

    # Close the rental. Add last position near to the end_point
    longitude = end_point['location']['coordinates'][0]
    latitude = end_point['location']['coordinates'][1]
    post_position(longitude, latitude, bike_id, request_header)
    end_rental = requests.post(
        url=URL + ACTION[2] + str(bike_id) + '/', data='{}', headers=request_header)
    print(end_rental.text)
    print("Response:", end_rental.status_code)


def latency_eval():
    mean = numpy.mean(latency)
    std = numpy.std(latency)
    min = numpy.min(latency)
    max = numpy.max(latency)
    print("Min:", min, "Max:", max, "Mean:", mean, "Std:", std)


if __name__ == "__main__":
    # Disable subjectAltName warning
    urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)

    # Load poi
    with open("poi_geofence.geojson") as f:
        geofences = json.load(f)['features']

    # Generate 30 rounds
    for i in range(0, ROUNDS):
        simulate_rental()
    latency_eval()
