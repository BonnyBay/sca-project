package com.sca.unibike.notification_services;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class NotificationInitializer extends Application {
    public static final String RENTAL_CHANNEL = "RentalServiceChannel";
    public static final String GEOFENCE_CHANNEL = "GeofenceChannel";

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel foregroundServiceChannel = new NotificationChannel(RENTAL_CHANNEL,
                    "Rental Service", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationChannel geofenceChannel = new NotificationChannel(GEOFENCE_CHANNEL, "Geofence",
                    NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(foregroundServiceChannel);
            manager.createNotificationChannel(geofenceChannel);
        }
    }
}
