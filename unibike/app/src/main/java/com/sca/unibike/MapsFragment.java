package com.sca.unibike;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.sca.unibike.data.model.Rastrelliera;
import com.sca.unibike.utils.SharedPrefUtils;
import com.sca.unibike.utils.VolleyUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapsFragment extends Fragment implements OnMapReadyCallback {
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private final byte FINE_LOCATION_REQUEST_CODE = 0;
    private MapView mapView;
    private GoogleMap googleMap;
    private Marker userPosition;
    private RequestQueue volleyRequestQueue;
    private final boolean viewResumed = false;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        userPosition = this.googleMap.addMarker(new MarkerOptions().position(new LatLng(0.0, 0.0))
                .title(getString(R.string.your_position))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        getRastrelliere();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        //fused location init
        buildLocationRequest();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    FINE_LOCATION_REQUEST_CODE);
        } else {
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        }
        volleyRequestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    @Override
    public void onDestroy() {
        //Se sono state effettuate rotazioni con un'altro fragment attivo mapViewv potrebbe essere
        //nullo
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull @NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapView != null) {
            mapView.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void getRastrelliere() {
        //Get user token, it the user is not logged in it hasn't any token. We display a toast and we not
        //fetch any rastrelliera.
        String userToken = SharedPrefUtils.getUserToken(getContext());
        if (userToken.isEmpty()) {
            Toast.makeText(getContext(), R.string.not_logged_warning, Toast.LENGTH_LONG).show();
        } else {
            //Fire api request and populate map
            String url = getResources().getString(R.string.backend_url).concat("rastrelliera/");
            JSONArray jsonBody = null;
            try {
                jsonBody = new JSONArray("[]");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Response.Listener<JSONArray> responseListener = new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    //parse json && show rastrelliere on map
                    Gson deserializer = new Gson();
                    Rastrelliera[] rastrelliere = deserializer.fromJson(response.toString(),
                            Rastrelliera[].class);
                    if (rastrelliere.length != 0) {
                        for (Rastrelliera rastrelliera : rastrelliere) {
                            BitmapDescriptor marker;
                            if (rastrelliera.getAvailabeBikes().size() > 0) {
                                marker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
                            } else {
                                marker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                            }
                            Marker rentalPoint = googleMap.addMarker(new MarkerOptions().position(rastrelliera.getLocation()
                                    .getLatLng()).title(getString(R.string.show_available_bikes))
                                    .icon(marker));
                            rentalPoint.setTag(rastrelliera);
                            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                @Override
                                public void onInfoWindowClick(Marker marker) {
                                    //available bike bundle creation and transition to new fragment.
                                    Bundle bundle = new Bundle();
                                    bundle.putIntegerArrayList("availableBikes", ((Rastrelliera) marker.getTag()).getAvailabeBikes());
                                    Navigation.findNavController(getActivity(), R.id.nav_host_fragment_content_main)
                                            .navigate(R.id.action_nav_map_to_availableBikesFragment, bundle);
                                }
                            });
                        }
                    } else {
                        Toast.makeText(getContext(), R.string.error_while_updating, Toast.LENGTH_LONG).show();
                    }
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getContext(), getString(R.string.request_failed)
                                    + "\n" + VolleyUtils.networkErrorBuilder(error),
                            Toast.LENGTH_LONG).show();
                }
            };
            JsonArrayRequest jsonArrayRequest = VolleyUtils.getAuthenticatedRequest(Request.Method.GET,
                    url, jsonBody, responseListener, errorListener, userToken);
            volleyRequestQueue.add(jsonArrayRequest);
        }
    }

    @SuppressLint("MissingPermission")
    private void buildLocationRequest() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //request location every 5 sec
        locationRequest.setInterval(5000);
        locationCallback = new LocationCallback() {
            boolean firstFix = true;

            @Override
            public void onLocationResult(@NonNull @NotNull LocationResult locationResult) {
                LatLng position = new LatLng(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude());
                userPosition.setPosition(position);
                if (firstFix) {
                    //Zoom only on first fix
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
                    firstFix = false;
                }
            }
        };
    }

    @SuppressLint("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case FINE_LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
                } else {
                    Toast.makeText(getActivity(), R.string.fine_location_permission_error,
                            Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }
}
