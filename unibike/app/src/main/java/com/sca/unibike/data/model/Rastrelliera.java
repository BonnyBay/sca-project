package com.sca.unibike.data.model;

import java.util.ArrayList;

public class Rastrelliera {
    private ArrayList<Integer> bicicletta;
    private final Location location;

    public Rastrelliera(ArrayList<Integer> availabeBikes, Location location) {
        this.bicicletta = bicicletta;
        this.location = location;
    }

    public ArrayList<Integer> getAvailabeBikes() {
        return bicicletta;
    }

    public Location getLocation() {
        return location;
    }
}
