package com.sca.unibike.ui.login;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.sca.unibike.R;
import com.sca.unibike.data.model.User;
import com.sca.unibike.databinding.FragmentLoginBinding;
import com.sca.unibike.utils.SharedPrefUtils;
import com.sca.unibike.utils.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends Fragment {

    private LoginViewModel loginViewModel;
    private FragmentLoginBinding binding;
    private EditText emailEditText;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private Button registerButton;
    private Button loginButton;
    private Button logoutButton;
    private ProgressBar loadingProgressBar;
    private RequestQueue volleyRequestQueue;
    private Gson deserializer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentLoginBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);
        volleyRequestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        emailEditText = binding.email;
        usernameEditText = binding.username;
        passwordEditText = binding.password;
        confirmPasswordEditText = binding.confirmPassword;
        registerButton = binding.register;
        loginButton = binding.login;
        logoutButton = binding.logout;
        loadingProgressBar = binding.loginLoading;
        deserializer = new Gson();
        loadData(); //If the user is already logged it fills the edit text
        loginViewModel.getLoginFormState().observe(getViewLifecycleOwner(), new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                registerButton.setEnabled(loginFormState.isDataValid().equals("Complete"));
                loginButton.setEnabled(loginFormState.isDataValid().equals("Partial"));
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getEmailError() != null) {
                    emailEditText.setError(getString(loginFormState.getEmailError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
                if (loginFormState.getConfirmPasswordError() != null) {
                    confirmPasswordEditText.setError(getString(loginFormState.getConfirmPasswordError()));
                }
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(), emailEditText.getText().toString(),
                        passwordEditText.getText().toString(), confirmPasswordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        emailEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        confirmPasswordEditText.addTextChangedListener(afterTextChangedListener);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeRegistration();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeLoginRequest();
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //delete shared prefs and clear input text
                purgeUserData();
                usernameEditText.setText("");
                emailEditText.setText("");
                logoutButton.setEnabled(false);
            }
        });
    }

    private void makeRegistration() {
        loadingProgressBar.setVisibility(View.VISIBLE);
        String url = getResources().getString(R.string.backend_url).concat("accounts/register");
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"username\":" + usernameEditText.getText().toString() + "," +
                    "\"email\":" + emailEditText.getText().toString() + "," +
                    "\"password\":" + passwordEditText.getText().toString() + "," +
                    "\"password2\":" + confirmPasswordEditText.getText().toString() + "}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loadingProgressBar.setVisibility(View.INVISIBLE);
                try {
                    //save credentials in shared pref.
                    User user = deserializer.fromJson(response.toString(), User.class);
                    saveData(user.getUsername(), user.getEmail(), user.getToken());
                    updateUiWithUser(new LoggedInUserView(usernameEditText.getText().toString()));
                    registerButton.setEnabled(false);
                    logoutButton.setEnabled(true);
                    passwordEditText.setText("");
                    confirmPasswordEditText.setText("");
                } catch (JsonSyntaxException e) {
                    showLoginFailed(getString(R.string.registration_error));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingProgressBar.setVisibility(View.INVISIBLE);
                showLoginFailed(getString(R.string.request_failed) + "\n" + VolleyUtils.networkErrorBuilder(error));
            }
        });
        volleyRequestQueue.add(stringRequest);
    }

    private void makeLoginRequest() {
        loadingProgressBar.setVisibility(View.VISIBLE);
        String url = getResources().getString(R.string.backend_url).concat("accounts/login");
        JSONObject jsonBody = null;
        try {
            //Gestione di Django account -> username e` la mail
            jsonBody = new JSONObject("{\"username\":" + emailEditText.getText().toString() + "," +
                    "\"password\":" + passwordEditText.getText().toString() + "}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loadingProgressBar.setVisibility(View.INVISIBLE);
                try {
                    //save credentials in shared pref.
                    User user = deserializer.fromJson(response.toString(), User.class);
                    saveData(usernameEditText.getText().toString(), emailEditText.getText().toString(),
                            user.getToken());
                    updateUiWithUser(new LoggedInUserView(usernameEditText.getText().toString()));
                    loginButton.setEnabled(false);
                    logoutButton.setEnabled(true);
                    passwordEditText.setText("");
                    //logoutButton.setVisibility(Button.VISIBLE);
                } catch (JsonSyntaxException e) {
                    showLoginFailed(getString(R.string.login_failed));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loadingProgressBar.setVisibility(View.INVISIBLE);
                //if the device is network response may be null if the device is offline or if the
                //request is not fired for some reason
                if (error.networkResponse != null && error.networkResponse.statusCode == 400) {
                    //if the response is error 400 the user has given wrong credentials
                    showLoginFailed(getString(R.string.wrong_credentials));
                } else {
                    //some other error, we "parse" the error
                    showLoginFailed(getString(R.string.request_failed) + "\n" + VolleyUtils.networkErrorBuilder(error));
                }
            }
        });
        volleyRequestQueue.add(jsonObjectRequest);
    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        if (getContext() != null && getContext().getApplicationContext() != null) {
            Toast.makeText(getContext().getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
        }
    }

    private void showLoginFailed(String errorCode) {
        if (getContext() != null && getContext().getApplicationContext() != null) {
            Toast.makeText(
                    getContext().getApplicationContext(),
                    errorCode,
                    Toast.LENGTH_LONG).show();
        }
    }

    private void saveData(String username, String email, String token) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("username", username);
        editor.putString("email", email);
        editor.putString("token", token);
        editor.apply();
    }

    private void loadData() {
        String token = SharedPrefUtils.getUserToken(getContext());
        if (!token.isEmpty()) {
            emailEditText.setText(SharedPrefUtils.getUserEmail(getContext()));
            usernameEditText.setText(SharedPrefUtils.getUserName(getContext()));
            loginButton.setEnabled(false);
            registerButton.setEnabled(false);
            logoutButton.setEnabled(true);
        }
    }

    private void purgeUserData() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}