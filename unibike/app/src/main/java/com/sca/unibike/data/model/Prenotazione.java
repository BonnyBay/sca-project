package com.sca.unibike.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Prenotazione {
    @SerializedName("id")
    private final int idPrenotazione;
    private final int user;
    @SerializedName("bicicletta")
    private final int idBicicletta;
    @SerializedName("codice_sblocco")
    private final int codiceSblocco;
    @SerializedName("giornata_prenotata")
    private final String giornataPrenotata;
    private ArrayList<Double> bikePosition;
    //Location name associated to coordinates in bike position
    private String locationName;

    public Prenotazione(int id, int user, int idBicicletta, int codiceSblocco, String giornataPrenotata) {
        this.idPrenotazione = id;
        this.user = user;
        this.idBicicletta = idBicicletta;
        this.codiceSblocco = codiceSblocco;
        this.giornataPrenotata = giornataPrenotata;
        locationName = "";
    }

    public int getIdPrenotazione() {
        return idPrenotazione;
    }

    public int getUser() {
        return user;
    }

    public int getIdBicicletta() {
        return idBicicletta;
    }

    public int getCodiceSblocco() {
        return codiceSblocco;
    }

    public String getGiornataPrenotata() {
        return giornataPrenotata;
    }

    public ArrayList<Double> getBikePosition() {
        return bikePosition;
    }

    public void setBikePosition(ArrayList<Double> bikePosition) {
        this.bikePosition = bikePosition;
    }

    public void setLocationName(String locationName) {
        //Geocoder may not be able to get location name for a specific set of coordinates, in this
        //case we set location name as latitude, longitude.
        if (locationName == null) {
            this.locationName = bikePosition.get(1) + "," + bikePosition.get(0);
        } else {
            this.locationName = locationName;
        }
    }

    public String getLocationName() {
        return locationName;
    }
}
