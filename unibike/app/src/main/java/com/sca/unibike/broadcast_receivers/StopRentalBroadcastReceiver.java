package com.sca.unibike.broadcast_receivers;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.sca.unibike.R;
import com.sca.unibike.notification_services.ForegroundRentalService;
import com.sca.unibike.utils.SharedPrefUtils;
import com.sca.unibike.utils.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class StopRentalBroadcastReceiver extends BroadcastReceiver {
    private String userToken;
    private String bikeId;
    private boolean rentalErrorStop;
    private RequestQueue volleyRequestQueue;

    @Override
    public void onReceive(Context context, Intent intent) {
        userToken = SharedPrefUtils.getUserToken(context);
        bikeId = intent.getStringExtra("bikeId");
        rentalErrorStop = intent.getBooleanExtra("rentalErrorStop", false);
        volleyRequestQueue = Volley.newRequestQueue(context);
        if (!rentalErrorStop) {
            sendParkingRequest(context);
        } else {
            context.stopService(new Intent(context, ForegroundRentalService.class));
        }
    }

    private void sendParkingRequest(Context context) {
        if (userToken.isEmpty()) {
            Toast.makeText(context, R.string.not_logged_warning, Toast.LENGTH_LONG).show();
        } else {
            String url = context.getResources().getString(R.string.backend_url).concat("termina_noleggio/" + bikeId + "/");
            JSONObject jsonBody = null;
            try {
                jsonBody = new JSONObject("{}");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String responseText = response.getString("Response");
                        switch (responseText) {
                            case "Bike parked in rental point with success.":
                                stopActivityRecognition(context);
                                Toast.makeText(context, R.string.bike_parked, Toast.LENGTH_LONG).show();
                            case "The bike is parked in this rental point yet":
                                context.stopService(new Intent(context, ForegroundRentalService.class));
                                break;
                            case "The bike is far away from rental point":
                                Toast.makeText(context, R.string.unable_to_park,
                                        Toast.LENGTH_LONG).show();
                                break;
                        }
                    } catch (JSONException e) {
                        Toast.makeText(context, R.string.error_parsing_server_response,
                                Toast.LENGTH_LONG).show();
                    }
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, R.string.parking_error,
                            Toast.LENGTH_LONG).show();
                }
            };
            JsonObjectRequest jsonObjectRequest = VolleyUtils.getAuthenticatedRequest(Request.Method.POST,
                    url, jsonBody, responseListener, errorListener, userToken);
            volleyRequestQueue.add(jsonObjectRequest);
        }
    }

    private void stopActivityRecognition(Context context) {
        PendingIntent pendingIntent = ForegroundRentalService.activityMonitoringPendingIntent;
        Task<Void> activityRecognition = ActivityRecognition.getClient(context).
                removeActivityTransitionUpdates(pendingIntent);
        activityRecognition.addOnSuccessListener(
                new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        Log.i("ACTIVITY_TYPE:", "Successfully stopped");
                        pendingIntent.cancel();
                    }
                }
        );
        activityRecognition.addOnFailureListener(
                new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Log.i("ACTIVITY_TYPE:", e.toString());
                    }
                }
        );
    }
}
