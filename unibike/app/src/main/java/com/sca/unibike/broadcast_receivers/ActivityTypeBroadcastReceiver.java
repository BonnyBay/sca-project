package com.sca.unibike.broadcast_receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.ActivityTransitionEvent;
import com.google.android.gms.location.ActivityTransitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.sca.unibike.utils.SharedPrefUtils;

import java.util.List;

public class ActivityTypeBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (ActivityTransitionResult.hasResult(intent)) {
            ActivityTransitionResult result = ActivityTransitionResult.extractResult(intent);
            List<ActivityTransitionEvent> results = result.getTransitionEvents();
            ActivityTransitionEvent activity = results.get(results.size()-1);
            if ((activity.getTransitionType() == ActivityTransition.ACTIVITY_TRANSITION_ENTER) &&
                    (activity.getActivityType() == DetectedActivity.ON_FOOT
                            || activity.getActivityType() == DetectedActivity.WALKING
                    || activity.getActivityType() == DetectedActivity.RUNNING
                    || activity.getActivityType() == DetectedActivity.STILL)) {
                SharedPrefUtils.setWalkingStatus(context, true);
            } else {
                SharedPrefUtils.setWalkingStatus(context, false);
            }
        }
    }
}
