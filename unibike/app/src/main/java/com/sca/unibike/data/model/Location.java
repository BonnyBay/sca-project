package com.sca.unibike.data.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class Location {
    private final String type;
    private final List<Double> coordinates;

    public Location(String type, List<Double> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public LatLng getLatLng() {
        return new LatLng(coordinates.get(1), coordinates.get(0));
    }
}
