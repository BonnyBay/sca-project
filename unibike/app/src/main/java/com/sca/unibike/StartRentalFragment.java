package com.sca.unibike;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.sca.unibike.notification_services.ForegroundRentalService;
import com.sca.unibike.utils.SharedPrefUtils;

import org.jetbrains.annotations.NotNull;

public class StartRentalFragment extends Fragment {
    private final byte FINE_LOCATION_REQUEST_CODE = 1;
    private final byte ACTIVITY_RECOGNITION_REQUEST_CODE = 2;
    private Button startRentalButton;
    private TextView bikeIdTextView;
    private boolean locationPermissions, userLoggedIn, bikeIdSet;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_start_rental, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bikeIdTextView = view.findViewById(R.id.bike_id_edit_text);
        bikeIdTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkRequirements();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        TextView bookingCodeTextView = view.findViewById(R.id.booking_code_edit_text);
        startRentalButton = view.findViewById(R.id.start_rental_button);
        startRentalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start rental foreground service
                Intent serviceStart = new Intent(getContext(), ForegroundRentalService.class);
                serviceStart.putExtra("bikeId", bikeIdTextView.getText().toString());
                serviceStart.putExtra("bookingCode", bookingCodeTextView.getText().toString());
                getActivity().startService(serviceStart);
            }
        });
        checkAndroidPermissions();
    }

    private void checkAndroidPermissions() {
        //Check gps permissions and activity recognition
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    FINE_LOCATION_REQUEST_CODE);
            locationPermissions = false;
        } else {
            locationPermissions = true;
        }
        /*Ask activity recognition permission only in case device is > Android 9
        * otherwise is automatically given*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACTIVITY_RECOGNITION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACTIVITY_RECOGNITION},
                    ACTIVITY_RECOGNITION_REQUEST_CODE);
        }
    }

    private void checkRequirements() {
        //The user is logged in
        if (SharedPrefUtils.getUserToken(getActivity()).isEmpty()) {
            userLoggedIn = false;
            Toast.makeText(getContext(), R.string.not_logged_warning, Toast.LENGTH_LONG).show();
        } else {
            userLoggedIn = true;
        }
        //The user has set bikeId
        bikeIdSet = !bikeIdTextView.getText().toString().isEmpty();
        startRentalButton.setEnabled(locationPermissions && userLoggedIn && bikeIdSet);
    }

    @SuppressLint("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case FINE_LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //unlock button
                    locationPermissions = true;
                } else {
                    Toast.makeText(getActivity(), R.string.fine_location_permission_error,
                            Toast.LENGTH_LONG).show();
                    locationPermissions = false;
                }
                //necessary in case user denies the gps permission, then tries to put the bike id.
                //In that case the app checks again the permissions in order to unlock the start rental button,
                //if the user denied the permission, without this boolean flag, the app will ask again and again
                //to give gps permissions.
                break;
            }
            case ACTIVITY_RECOGNITION_REQUEST_CODE: {
                break;
            }
        }
    }
}
