package com.sca.unibike.ui.recycler_views;

import android.app.Activity;
import android.content.Context;
import android.location.Geocoder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.sca.unibike.R;
import com.sca.unibike.data.model.Prenotazione;
import com.sca.unibike.utils.SharedPrefUtils;
import com.sca.unibike.utils.VolleyUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class BookedBicyclesAdapter extends RecyclerView.Adapter<BookedBicyclesAdapter.BookedBicyclesViewHolder> {
    private List<Prenotazione> prenotazioni;
    private final Context originContext;
    private final Activity originActivity;
    private String userToken;
    private Gson deserializer;
    private final RequestQueue volleyRequestQueue;
    private final Geocoder geocoder;

    public static class BookedBicyclesViewHolder extends RecyclerView.ViewHolder {
        public TextView bikeId;
        public TextView bookingCode;
        public TextView bikePosition;
        public TextView bookedDay;
        public Button deleteReservation;

        public BookedBicyclesViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            bikeId = itemView.findViewById(R.id.bike_id);
            bookingCode = itemView.findViewById(R.id.booking_code_textview);
            bikePosition = itemView.findViewById(R.id.current_bike_position);
            deleteReservation = itemView.findViewById(R.id.unbook_button);
            bookedDay = itemView.findViewById(R.id.booked_day);
        }
    }

    public BookedBicyclesAdapter(Activity originActivity, Context originContext) {
        this.originContext = originContext;
        this.originActivity = originActivity;
        volleyRequestQueue = Volley.newRequestQueue(originContext);
        prenotazioni = new ArrayList<Prenotazione>();
        geocoder = new Geocoder(originActivity, Locale.getDefault());
        //fire api request and build list
        getUserBookings();
    }

    @NonNull
    @NotNull
    @Override
    public BookedBicyclesViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.booked_bike, parent, false);
        BookedBicyclesViewHolder viewHolder = new BookedBicyclesViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull BookedBicyclesViewHolder holder, int position) {
        Prenotazione currentPrenotazione = prenotazioni.get(position);
        holder.bikeId.setText("" + currentPrenotazione.getIdBicicletta());
        holder.bookingCode.setText("" + currentPrenotazione.getCodiceSblocco());
        holder.bookedDay.setText(currentPrenotazione.getGiornataPrenotata());
        holder.bikePosition.setText(currentPrenotazione.getLocationName());
        holder.bikePosition.setSelected(true);
        holder.deleteReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteReservation(currentPrenotazione.getIdPrenotazione(), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return prenotazioni.size();
    }

    private void getUserBookings() {
        userToken = SharedPrefUtils.getUserToken(originContext);
        if (userToken.isEmpty()) {
            Toast.makeText(originContext, R.string.not_logged_warning, Toast.LENGTH_LONG).show();
        } else {
            //Fire api request and populate map
            originActivity.findViewById(R.id.loading_booked_bikes).setVisibility(View.VISIBLE);
            String url = originActivity.getString(R.string.backend_url).concat("prenotazione_utente/");
            JSONArray jsonBody = null;
            try {
                jsonBody = new JSONArray("[{\"username\":" + SharedPrefUtils.getUserEmail(originActivity) + "}]");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Response.Listener<JSONArray> responseListener = new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    //parse json && show prenotazioni
                    deserializer = new Gson();
                    prenotazioni = new ArrayList<Prenotazione>(Arrays.asList(deserializer.fromJson(
                            response.toString(), Prenotazione[].class)));
                    if (prenotazioni.size() != 0) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                for (Prenotazione prenotazione : prenotazioni) {
                                    getPosizioneBicicletta(prenotazione);
                                }
                            }
                        }).run();
                    } else {
                        //hide progress bar only if we not have to fire the second api request
                        originActivity.findViewById(R.id.loading_booked_bikes).setVisibility(View.INVISIBLE);
                    }
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(originContext, originActivity.getString(R.string.request_failed)
                                    + "\n" + VolleyUtils.networkErrorBuilder(error),
                            Toast.LENGTH_LONG).show();
                }
            };
            JsonArrayRequest jsonArrayRequest = VolleyUtils.getAuthenticatedRequest(Request.Method.GET,
                    url, jsonBody, responseListener, errorListener, userToken);
            volleyRequestQueue.add(jsonArrayRequest);
        }
    }

    private void getPosizioneBicicletta(Prenotazione prenotazione) {
        ArrayList<Double> bikePosition = new ArrayList<Double>();
        if (userToken.isEmpty()) {
            Toast.makeText(originContext, R.string.not_logged_warning, Toast.LENGTH_SHORT).show();
        } else {
            String url = originActivity.getString(R.string.backend_url).concat("cerca_bicicletta/" +
                    prenotazione.getIdBicicletta() + "/");
            JSONObject jsonBody = null;
            try {
                jsonBody = new JSONObject("{}");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //parse json && add coordinates
                    try {
                        ArrayList<Double> coordinates = new ArrayList<Double>(2);
                        coordinates.add(0, (Double) response.getJSONArray("coordinates").get(0));
                        coordinates.add(1, (Double) response.getJSONArray("coordinates").get(1));
                        prenotazione.setBikePosition(coordinates);
                        //geocoding location name from coordinates
                        prenotazione.setLocationName(geocoder.getFromLocation(coordinates.get(1),
                                coordinates.get(0), 1).get(0).getAddressLine(0));
                    } catch (JSONException | IOException e) {
                        Toast.makeText(originContext, R.string.error_retrieving_bike_position, Toast.LENGTH_LONG).show();
                    } finally {
                        originActivity.findViewById(R.id.loading_booked_bikes).setVisibility(View.INVISIBLE);
                        notifyDataSetChanged();
                    }
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(originContext, originActivity.getString(R.string.request_failed)
                                    + "\n" + VolleyUtils.networkErrorBuilder(error),
                            Toast.LENGTH_LONG).show();
                }
            };
            JsonObjectRequest jsonObjectRequest = VolleyUtils.getAuthenticatedRequest(Request.Method.GET,
                    url, jsonBody, responseListener, errorListener, userToken);
            volleyRequestQueue.add(jsonObjectRequest);
        }
    }

    private void deleteReservation(int bookingId, int cardPosition) {
        if (userToken.isEmpty()) {
            Toast.makeText(originContext, R.string.not_logged_warning, Toast.LENGTH_LONG).show();
        } else {
            String url = originActivity.getString(R.string.backend_url).concat("cancella_prenotazione/" +
                    bookingId + "/");
            JSONObject jsonBody = null;
            try {
                jsonBody = new JSONObject("{}");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getString("Response").toString().equals("Reservation has been deleted.")) {
                            prenotazioni.remove(cardPosition);
                            notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(originContext, R.string.error_deleting_booking, Toast.LENGTH_LONG).show();
                    }
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(originContext, originActivity.getString(R.string.request_failed)
                                    + "\n" + VolleyUtils.networkErrorBuilder(error),
                            Toast.LENGTH_LONG).show();
                }
            };
            JsonObjectRequest jsonObjectRequest = VolleyUtils.getAuthenticatedRequest(Request.Method.DELETE,
                    url, jsonBody, responseListener, errorListener, userToken);
            volleyRequestQueue.add(jsonObjectRequest);
        }
    }
}
