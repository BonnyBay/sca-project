package com.sca.unibike.notification_services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.location.ActivityTransitionRequest;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.sca.unibike.MainActivity;
import com.sca.unibike.R;
import com.sca.unibike.broadcast_receivers.ActivityTypeBroadcastReceiver;
import com.sca.unibike.broadcast_receivers.StopRentalBroadcastReceiver;
import com.sca.unibike.utils.SharedPrefUtils;
import com.sca.unibike.utils.VolleyUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.sca.unibike.notification_services.NotificationInitializer.RENTAL_CHANNEL;

public class ForegroundRentalService extends Service {
    private String userToken, bikeId, bookingCode;
    private RequestQueue volleyRequestQueue;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Intent stopRentalIntent;
    private PendingIntent stopRentalPendingIntent;
    private NotificationManagerCompat notificationManager;
    private boolean walking;
    public static PendingIntent activityMonitoringPendingIntent;

    @Override
    /*
     * Triggered only when service is created the first time
     */
    public void onCreate() {
        super.onCreate();
        userToken = SharedPrefUtils.getUserToken(getApplicationContext());
        volleyRequestQueue = Volley.newRequestQueue(getApplicationContext());
        notificationManager = NotificationManagerCompat.from(this);
    }

    @Override
    /*
     * Triggered only when service is destroyed
     */
    public void onDestroy() {
        super.onDestroy();
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }


    @Override
    /*
     * Triggered everytime the service is started
     */
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Intent inserted in the notification. When clicked it opens the main activity.
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent startRentalPendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        bikeId = intent.getStringExtra("bikeId");
        bookingCode = intent.getStringExtra("bookingCode");
        if (bookingCode.isEmpty()) {
            bookingCode = "\"\""; //send empty code in Json
        }
        stopRentalIntent = new Intent(this, StopRentalBroadcastReceiver.class);
        stopRentalIntent.putExtra("userToken", userToken);
        stopRentalIntent.putExtra("bikeId", bikeId);
        //add to bundle rentalId, last position etc etc
        stopRentalPendingIntent = PendingIntent.getBroadcast(this, 1,
                stopRentalIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this, RENTAL_CHANNEL)
                .setContentTitle("Rental Service")
                .setContentText(getString(R.string.you_started_rental) + " " + bikeId)
                .setSmallIcon(R.drawable.ic_booked_bike)
                .setContentIntent(startRentalPendingIntent)
                .addAction(R.drawable.ic_lock, getString(R.string.end_rental), stopRentalPendingIntent)
                .build();
        startForeground(1, notification);
        buildLocationRequest();
        transitionListenerSetup();
        return START_REDELIVER_INTENT;
    }

    private void transitionListenerSetup() {
        //Setup of transition listener only if permission is given
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACTIVITY_RECOGNITION)
                == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getApplicationContext(),
                "com.google.android.gms.permission.ACTIVITY_RECOGNITION") == PackageManager.PERMISSION_GRANTED) {
            ArrayList<ActivityTransition> transitions = new ArrayList<ActivityTransition>();
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.WALKING)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.WALKING)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.ON_FOOT)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.ON_FOOT)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.RUNNING)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.RUNNING)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.STILL)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.STILL)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.ON_BICYCLE)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                    .build());
            transitions.add(new ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.ON_BICYCLE)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                    .build());
            ActivityTransitionRequest transitionRequest = new ActivityTransitionRequest(transitions);
            Intent activityRecognitionIntent = new Intent(this, ActivityTypeBroadcastReceiver.class);
            activityMonitoringPendingIntent = PendingIntent.getBroadcast(this, 5,
                    activityRecognitionIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Task<Void> activityRecognition = ActivityRecognition.getClient(this)
                    .requestActivityTransitionUpdates(transitionRequest, activityMonitoringPendingIntent);
            activityRecognition.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void result) {
                    Log.i("ACTIVITY_TYPE:", "Successfully started");
                }
            });
            activityRecognition.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull @NotNull Exception e) {
                    Log.i("ACTIVITY_TYPE:", e.toString());
                }
            });
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("MissingPermission")
    private void buildLocationRequest() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //request location every 5 sec
        locationRequest.setInterval(5000);
        locationCallback = new LocationCallback() {
            double latitude, longitude;

            @Override
            public void onLocationResult(@NonNull @NotNull LocationResult locationResult) {
                latitude = locationResult.getLastLocation().getLatitude();
                longitude = locationResult.getLastLocation().getLongitude();
                //fire api post
                postPosition(latitude, longitude);
            }
        };
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }

    private void fireGeofenceNotification(String geofenceMessage, String geofenceType) {
        String title = "Unknown area";
        if (geofenceType.equals("POI")) {
            title = getString(R.string.entered_geofence);
        } else {
            title = getString(R.string.entered_restricted);
        }
        Notification notification = new NotificationCompat.Builder(this, NotificationInitializer.GEOFENCE_CHANNEL)
                .setSmallIcon(R.drawable.ic_fence)
                .setContentTitle(title)
                .setContentText(geofenceMessage)
                .setNotificationSilent()
                .build();
        //Id is constant in order to "updating the notification" and not fire it again and again
        notificationManager.notify(0, notification);
    }

    private void postPosition(double latitude, double longitude) {
        if (userToken.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.not_logged_warning, Toast.LENGTH_LONG).show();
        } else {
            String url = getResources().getString(R.string.backend_url).concat("noleggio_utente/" + bikeId + "/");
            JSONObject jsonBody = null;
            try {
                jsonBody = new JSONObject("{\"codice_sblocco\":" + bookingCode + "," +
                        "\"location\": { \"type\": \"Point\", \"coordinates\": [" + longitude + "," +
                        latitude + "]}}"); //Coordinate in GeoJSON
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String responseText = response.getString("Response");
                        switch (responseText) {
                            case "Rental position update with success.":
                                if (response.has("Message")) {
                                    String geofenceMessage = response.getString("Message");
                                    //notify only if user is in walking status. Note that walking status is true for default, this
                                    //in case the user hasn't given the permission for activity type monitoring
                                    if (!geofenceMessage.isEmpty() && SharedPrefUtils.getWalkingStatus(getApplicationContext())) {
                                        fireGeofenceNotification(geofenceMessage, response.getString("Type"));
                                    }
                                }
                                break;
                            case "Error. This bike is not your.":
                            case "Bike not found in rental point. Maybe somebody is using it.":
                                Toast.makeText(getApplicationContext(), R.string.rented_by_another_user,
                                        Toast.LENGTH_LONG).show();
                                //kill service, critical error
                                stopRentalIntent.putExtra("rentalErrorStop", true);
                                sendBroadcast(stopRentalIntent);
                                break;
                            case "Maybe the inserted code is incorrect or you have not booked this bike today.":
                            case "Error. This bike is booked.":
                                Toast.makeText(getApplicationContext(), R.string.booked_by_another_user,
                                        Toast.LENGTH_LONG).show();
                                stopRentalIntent.putExtra("rentalErrorStop", true);
                                sendBroadcast(stopRentalIntent);
                                break;
                        }
                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), R.string.error_parsing_server_response,
                                Toast.LENGTH_LONG).show();
                        stopRentalIntent.putExtra("rentalErrorStop", true);
                        sendBroadcast(stopRentalIntent);
                    }
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), R.string.error_posting_position,
                            Toast.LENGTH_LONG).show();
                    stopRentalIntent.putExtra("rentalErrorStop", true);
                    sendBroadcast(stopRentalIntent);
                }
            };
            JsonObjectRequest jsonObjectRequest = VolleyUtils.getAuthenticatedRequest(Request.Method.POST,
                    url, jsonBody, responseListener, errorListener, userToken);
            volleyRequestQueue.add(jsonObjectRequest);
        }
    }
}

