package com.sca.unibike.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPrefUtils {

    public static String getUserToken(Context origin) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(origin);
        return sharedPreferences.getString("token", "");
    }

    public static String getUserEmail(Context origin) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(origin);
        return sharedPreferences.getString("email", "");
    }

    public static String getUserName(Context origin) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(origin);
        return sharedPreferences.getString("username", "");
    }

    public static void setWalkingStatus(Context origin, boolean status) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(origin);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("walking", status);
        editor.apply();
    }

    public static boolean getWalkingStatus(Context origin) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(origin);
        return sharedPreferences.getBoolean("walking", true);
    }
}
