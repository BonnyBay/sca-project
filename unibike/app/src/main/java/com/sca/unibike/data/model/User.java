package com.sca.unibike.data.model;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class User {

    private final String username;
    private final String email;
    private final String token;

    //Registration constructor
    public User(String username, String email, String token) {
        this.username = username;
        this.email = email;
        this.token = token;
    }

    //Login constructor
    public User(String token) {
        this.username = "";
        this.email = "";
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }
}