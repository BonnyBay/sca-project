package com.sca.unibike.utils;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyUtils {
    public static String networkErrorBuilder(VolleyError error) {
        String networkError = "";
        //in case the device is offline. The network response is null.
        if (error.networkResponse != null) {
            networkError += error.networkResponse.statusCode;
        }
        return networkError + " " + error.getMessage();
    }

    //JSON object request "factory" that returns an authenticated JSON object request with the user token.
    public static JsonObjectRequest getAuthenticatedRequest(int method, String url, JSONObject jsonBody,
                                                            Listener<JSONObject> responseListener,
                                                            ErrorListener errorListener,
                                                            String userToken) {
        JsonObjectRequest authenticatedRequest = new JsonObjectRequest(method, url, jsonBody, responseListener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Token ".concat(userToken));
                return headers;
            }
        };
        return authenticatedRequest;
    }

    public static JsonArrayRequest getAuthenticatedRequest(int method, String url, JSONArray jsonBody,
                                                           Listener<JSONArray> responseListener,
                                                           ErrorListener errorListener,
                                                           String userToken) {
        JsonArrayRequest authenticatedRequest = new JsonArrayRequest(method, url, jsonBody, responseListener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Token ".concat(userToken));
                return headers;
            }
        };
        return authenticatedRequest;
    }
}
