package com.sca.unibike.ui.recycler_views;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sca.unibike.R;
import com.sca.unibike.utils.SharedPrefUtils;
import com.sca.unibike.utils.VolleyUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class AvailableBikeAdapter extends RecyclerView.Adapter<AvailableBikeAdapter.AvailableBikeViewHolder> implements DatePickerDialog.OnDateSetListener {
    private final ArrayList<Integer> bikeList;
    private final Context originContext;
    private final Activity originActivity;
    private String bikeId;
    private final DatePickerDialog datePickerDialog;

    public static class AvailableBikeViewHolder extends RecyclerView.ViewHolder {
        private final TextView bikeId;
        private final Button bookButton;

        public AvailableBikeViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            bikeId = itemView.findViewById(R.id.bike_id);
            bookButton = itemView.findViewById(R.id.book_button);
        }
    }

    public AvailableBikeAdapter(ArrayList<Integer> bikeList, Activity originActivity, Context originContext) {
        this.bikeList = bikeList;
        this.originContext = originContext;
        this.originActivity = originActivity;
        this.datePickerDialog = setupDatePicker();
    }

    @NonNull
    @NotNull
    @Override
    public AvailableBikeViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup viewGroup, int i) {
        View bikeCard = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.available_bike,
                viewGroup, false);
        AvailableBikeViewHolder availableBikeViewHolder = new AvailableBikeViewHolder(bikeCard);
        return availableBikeViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AvailableBikeViewHolder availableBikeViewHolder, int i) {
        Integer currentItem = bikeList.get(i);
        availableBikeViewHolder.bikeId.setText(currentItem.toString());
        availableBikeViewHolder.bookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bikeId = availableBikeViewHolder.bikeId.getText().toString();
                //show date picker
                datePickerDialog.show();
            }
        });
    }

    private DatePickerDialog setupDatePicker() {
        Calendar mCalendar = Calendar.getInstance();
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);
        int month = mCalendar.get(Calendar.MONTH);
        int year = mCalendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(originContext, this::onDateSet,
                year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month += 1;
        String chosenDate = year + "-" + month + "-" + dayOfMonth;
        String userToken = SharedPrefUtils.getUserToken(originContext);
        if (userToken.isEmpty()) {
            Toast.makeText(originContext, R.string.not_logged_warning, Toast.LENGTH_LONG).show();
        } else {
            //Fire api request
            String url = originActivity.getString(R.string.backend_url).concat("prenotazione_utente/");
            JSONObject jsonBody = null;
            try {
                jsonBody = new JSONObject("{\"user\":" + SharedPrefUtils.getUserEmail(originActivity) + "," +
                        "\"bicicletta\":" + bikeId + "," +
                        "\"giornata_prenotata\":" + chosenDate + "}");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //parse json && show status
                    if (response.has("codice_sblocco")) {
                        Toast.makeText(originContext, R.string.booking_successful,
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(originContext, R.string.unknown_booking_error, Toast.LENGTH_LONG).show();
                    }
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse.statusCode == 400) {
                        Toast.makeText(originContext, R.string.booking_unsuccessful,
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(originContext, originActivity.getString(R.string.request_failed)
                                        + "\n" + VolleyUtils.networkErrorBuilder(error),
                                Toast.LENGTH_LONG).show();
                    }
                }
            };
            JsonObjectRequest jsonObjectRequest = VolleyUtils.getAuthenticatedRequest(Request.Method.POST,
                    url, jsonBody, responseListener, errorListener, userToken);
            RequestQueue volleyRequestQueue = Volley.newRequestQueue(originContext);
            volleyRequestQueue.add(jsonObjectRequest);
        }

    }

    @Override
    public int getItemCount() {
        return bikeList.size();
    }

}
