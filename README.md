# Progetto di Context-Awareness AA. 2020/2021

|                                      Website                                       |                         App Android                         |
| :--------------------------------------------------------------------------------: | :---------------------------------------------------------: |
|             ![Homepage amministratore](file_supporto/images/home.png)              | ![Mappa rastrelliere](file_supporto/images/device-map.png)  |
| ![Visualizzazione clustering schermata admin](file_supporto/images/clustering.png) | ![Service nolgggio](file_supporto/images/device-rental.png) |

## Per eseguire il progetto

1. Creare un Python virtual environment nella root del progetto.

   `python3 -m venv sca_env`

2. Installare le dipendenze nel file requirements.txt.

   `pip3 install -r requirements.txt`

3. Configurare un webserver a propria scelta. Si faccia riferimento a: [Depolying Django](https://docs.djangoproject.com/en/3.2/howto/deployment/)

### Note

Nella cartella unibike e`` presente il client Android per la app.

## Autori

Riccardo Mioli

Michele Bonini

## Università di Bologna - Informatica
