from django.urls import path
from accounts.api.views import(
    registration_view,
)
from rest_framework.authtoken.views import obtain_auth_token

from django.contrib.auth import views

app_name = "accounts"
from .forms import LoginForm

urlpatterns = [
    #registrazione
	path('register', registration_view, name="register"),
    #parte inerente all'auth token generato durante la registrazione
    path('login', obtain_auth_token, {'authentication_form':LoginForm}), # i login vanno indirizzati a questo url, ci penserà django a gestirlo con i tokns

]