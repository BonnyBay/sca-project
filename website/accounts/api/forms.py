from django.contrib.auth.forms import AuthenticationForm
from django import forms

class LoginForm (AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control',}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control',}))


#class NameForm(forms.Form):
    #your_name = forms.CharField(label='Your nameeeeee', max_length=100)