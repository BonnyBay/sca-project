from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes 
from rest_framework.permissions import IsAuthenticated
from accounts.api.serializers import RegistrationSerializer
from rest_framework.authtoken.models import Token
from django.contrib import messages




@api_view(['POST', ])
def registration_view(request):
    if request.method == 'POST':
        serializer = RegistrationSerializer(data=request.data)
        data = {}
        if serializer.is_valid(): #se matchano ...
            account = serializer.save()
            data['response'] = "Registrazione avvenuta con successo"
            data['email'] = account.email
            data['username'] = account.username
            #mostro anche il token quando uno si registra
            token = Token.objects.get(user=account).key
            data['token'] = token
            return Response(data, status=status.HTTP_200_OK)
        else:
            data = serializer.errors
            #print("sono qua",serializer.errors)
       
       
