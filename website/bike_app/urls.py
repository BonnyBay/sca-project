from django.urls import include, path
from rest_framework import routers
from . import views
from .api import views as views2  # queste views si trovano nella cartella api
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view


router = routers.DefaultRouter()


router.register(r'api/geofence', views2.GeofenceViewSet)
router.register(r'api/rastrelliera', views2.RastrellieraViewSet)


router.register(r'api/prenotazione', views2.PrenotazioneViewSet)


urlpatterns = [
    path('index/', views.index, name='index'),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # ci da accesso agli url di: login/logout/cambia password ecc ecc
    path("accounts/", include("django.contrib.auth.urls")),



]
