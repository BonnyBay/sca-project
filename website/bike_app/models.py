from django.contrib.gis.db.models.fields import PolygonField
from django.db import models
from django.contrib.gis.geos import Point, Polygon
from django.contrib.gis.db import models
#from django.contrib.auth.models import User
from django.conf import settings

from django.contrib.auth import get_user_model


class Bicicletta(models.Model):
    user_noleggio = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="user", through='Noleggio')

    # un utente può prenotare piu biciclette, e una bicicletta può essere prenotata da piu utenti
    user = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="user_booking", through='Prenotazione')

    def __str__(self):
        return '{}'.format(self.id)


class Prenotazione(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.DO_NOTHING)
    bicicletta = models.ForeignKey(Bicicletta,  on_delete=models.DO_NOTHING)
    codice_sblocco = models.IntegerField(unique=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    giornata_prenotata = models.CharField(
        max_length=50, blank=True, null=False)

    def __str__(self):
        return '{}, {}, {}, {}, {}'.format(self.user, self.bicicletta, self.codice_sblocco, self.timestamp, self.id, self.giornata_prenotata)


class Rastrelliera(models.Model):
    location = models.PointField(
        geography=True, default=Point(11.3426163, 44.494887))
    bicicletta = models.ManyToManyField(Bicicletta, blank=True)
    nome = models.CharField(max_length=250, blank=True, null=False)

    def __str__(self):
        return 'coordinate:[{}], id_rastrelliera:({}), nome:{}'.format(self.location,
                                                                       self.id,
                                                                       self.nome)


class Noleggio(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.DO_NOTHING)
    bicicletta = models.ForeignKey(
        Bicicletta, related_name="noleggi", on_delete=models.DO_NOTHING)
    timestamp = models.DateTimeField(auto_now_add=True)
    location = models.PointField(geography=True, default=Point(
        11.3426163, 44.494887))  # posizione utente con bici 

    def __str__(self):
        return 'coordinate:[{}], {}, {}, {}, {} '.format(self.location,
                                                         self.timestamp,
                                                         self.user,
                                                         self.bicicletta,
                                                         self.id)


class Geofence(models.Model):
    GEOFENCE_CHOICES = [("POI", "POI"), ("RESTRICTED", "RESTRICTED")]
    geofence_area = models.PolygonField(geography=True)
    messaggio = models.CharField(max_length=200)
    tipologia = models.CharField(max_length=30,
                                 choices=GEOFENCE_CHOICES,
                                 default="POI")

    def __str__(self):
        return 'geofence_area:[{}], messaggio:({}), tipologia:({}) '.format(self.geofence_area,
                                                                            self.messaggio,
                                                                            self.tipologia, self.id)

    class meta:
        db_table = "geofence"
