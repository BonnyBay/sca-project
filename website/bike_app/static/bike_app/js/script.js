/***********************************************************************************
 *** Inizializzazione della mappa e delle icone (inizializzaten nell index.html) ***
 ***********************************************************************************/
var map = L.map('map',{"tap":false}).setView([44.494887, 11.3426163], 13);


var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
var LeafIcon = L.Icon.extend({
    options: {
    iconSize:     [38, 70],
    shadowSize:   [50, 64],
    iconAnchor:   [20, 65],
    shadowAnchor: [4, 62],
    popupAnchor:  [-3, -56]
    }
});
var rastrelliera_icon = new LeafIcon({
    iconUrl: 'http://leafletjs.com/examples/custom-icons/leaf-green.png',
    });
var bici_icon = new LeafIcon({
    iconUrl: icona_bicicletta,
});
var posizione_icon = new LeafIcon({
    iconUrl: icona_posizione,
});
var partenza_icon = new LeafIcon({
    iconUrl: icona_partenza,
});


/***********************************************************************
 *** Passo al backend l'ID della bici selezionata nel menu a tendina ***
 ***********************************************************************/
//recupero il numero di cluster selezionati dallo slider
const $valueSpan = $('.valueSpan2');
const $value = $('#customRange11');
$valueSpan.html($value.val());
$value.on('input change', () => {
    $valueSpan.html($value.val());
});

var url_href = window.location.href;
var url = new URL(url_href);
$("#btn").click( function() {
    var sel = document.getElementById('FormControlSelect1');
    var data_inserita = $('#datepicker').val();
    window.location = "https://norwegiangoat.ddns.net/bike_app/index/?ncluster="+parseInt($value.val())+"&bike_id=" + parseInt(sel.value)+"&data="+data_inserita;
});


/*****************************************************
 *** Gestione del menu a tendina con id delle bici ***
 *****************************************************/

var bici_selezionata = url.searchParams.get("bike_id");
//riempio lo spinner per scegliere la bicicletta con tutti gli id presenti nel database
for(var i=0; i<jsonObject_biciclette.length; i++){
    var o = new Option(jsonObject_biciclette[i].id, jsonObject_biciclette[i].id); //option text & value, in questo caso sono uguali
    $(o).html(jsonObject_biciclette[i].id);
    $("#FormControlSelect1").append(o);
}
//gestisco e ri-aggiorn oil select-option del menu a tendina delle biciclette
if (bici_selezionata!=null){
    document.getElementById('FormControlSelect1').value = bici_selezionata
}



/*********************************************************
 *** Gestione e inizializzazione del picker della data ***
 *********************************************************/
var data_selezionata = url.searchParams.get("data");
$(function () {
    $("#datepicker").datepicker({ 
        autoclose: true
    });    
});

if (data_selezionata!=null){
    $('.data_inserita').val(data_selezionata)
    console.log(data_selezionata)
}else{
    $('.data_inserita').val("") 
}

/*******************************************************
 *** Visualizzazione TUTTE le posizioni attuali bici ***
 *******************************************************/
var markerGroup_last_position = L.layerGroup().addTo(map);
function valueChanged(){
    var vett_marker_bike = []
    for(var i = 0; i< vettore_id_bici_noleggi.length; i++){
        var marker_noleggio = L.marker([vettore_noleggi_posizion_lat[i],vettore_noleggi_posizion_long[i]],{
            icon: bici_icon
        })
        vett_marker_bike.push(marker_noleggio)
    }

    if($('.check_all').is(":checked")){ 
        //show
        for (var i=0; i<vett_marker_bike.length; i++){
            vett_marker_bike[i].addTo(markerGroup_last_position)
            vett_marker_bike[i].bindPopup('<p> Bike ID: '+vettore_id_bici_noleggi[i] +'</p>')
        }
    }else{
        //hide
        markerGroup_last_position.clearLayers();
    }
}
if($('.check_all').is(":checked")){
    valueChanged()
}else if($('.check_single').is(":checked")){
    valueChanged2()
}

//visualizzazione TUTTE le posizioni delle biciclette (noleggi)
var markerGroup_all_position = L.layerGroup().addTo(map);
var firstpolyline = null
function valueChanged2(){
    var single_point = 0
    var vettore_LatLng_points = []
    if (jsonObject_single_pos != null){
        var vett_marker_bike_all = []
        for(var i=0; i<jsonObject_single_pos.length; i++){
            var marker_noleggio = null;
            if (i==jsonObject_single_pos.length-1){ //aggiungo l'icona
                var marker_noleggio = L.marker([jsonObject_single_pos[i].location.coordinates[1],jsonObject_single_pos[i].location.coordinates[0]],{
                    icon: bici_icon
                });
                vett_marker_bike_all.push(marker_noleggio.bindPopup('<p>Bike ID: '+jsonObject_single_pos[i].bicicletta+'<br>Data: '+jsonObject_single_pos[i].timestamp+'<br>User: '+ jsonObject_single_pos[i].user_noleggio+'</p>'))

            }else if(i==0){ //è la partenza
                var marker_noleggio = L.marker([jsonObject_single_pos[i].location.coordinates[1],jsonObject_single_pos[i].location.coordinates[0]],{
                    icon: partenza_icon
                });
                vett_marker_bike_all.push(marker_noleggio.bindPopup('<p>Bike ID: '+jsonObject_single_pos[i].bicicletta+'<br>Data: '+jsonObject_single_pos[i].timestamp+'<br>User: '+ jsonObject_single_pos[i].user_noleggio+'</p>'))
            }
            if (i>=1 && jsonObject_single_pos[i].user_noleggio!=jsonObject_single_pos[i-1].user_noleggio){
                var marker_noleggio = L.marker([jsonObject_single_pos[i-2].location.coordinates[1],jsonObject_single_pos[i-2].location.coordinates[0]]);
                vett_marker_bike_all.push(marker_noleggio.bindPopup('<p>Bike ID: '+jsonObject_single_pos[i-2].bicicletta+'<br>Data: '+jsonObject_single_pos[i-2].timestamp+'<br>User: '+ jsonObject_single_pos[i-2].user_noleggio+'</p>'))
            } 

            single_point = new L.LatLng(jsonObject_single_pos[i].location.coordinates[1], jsonObject_single_pos[i].location.coordinates[0]); 
            vettore_LatLng_points.push(single_point)
        }

        //costruzione polyline
        firstpolyline = new L.Polyline(vettore_LatLng_points, {
            color: 'red',
            weight: 4,
            opacity: 0.5,
            smoothFactor: 1
        });
        firstpolyline.addTo(markerGroup_all_position);
    }

    /**************************************************
     *** Tutte le posizioni di QUELLA bici mostrate ***
    ***************************************************/
    if($('.check_single').is(":checked")){ 
       
        //show
        for (var i=0; i<vett_marker_bike_all.length; i++){
            vett_marker_bike_all[i].addTo(markerGroup_all_position)
        }
     
    }else{
        //hide
        markerGroup_all_position.clearLayers();
        markerGroup_all_position.removeLayer(firstpolyline)
    }
}


/****************************************************************
 *** mostro le rastrelliere sulla mappa al click del checkbox ***
 ****************************************************************/
var markerGroup_rental = L.layerGroup().addTo(map);
var mostra_distanza_massima = false

function showRentalPoint(){
    var vett_latitudine_rast = []
    var vett_longitudine_rast = []
    var vett_dist_min_circle = []
    for (var i = 0; i < jsonObject_rastrelliere.length; i++) {
        vett_longitudine_rast.push(jsonObject_rastrelliere[i].location.coordinates[0])
        vett_latitudine_rast.push(jsonObject_rastrelliere[i].location.coordinates[1])
    }
    var marker_rastrelliere_vettore = []
    for(var i=0; i<vett_latitudine_rast.length;i++){
        //aggiungo i marker e i circle per le rastrelliere
        var marker_rastrelliere = L.marker([parseFloat(vett_latitudine_rast[i]).toFixed(6),parseFloat(vett_longitudine_rast[i]).toFixed(6)],{
            icon: posizione_icon
        })
        marker_rastrelliere_vettore.push(marker_rastrelliere)

        //area di 50m, rappresentate la distanza minima 
        var circle_rastrelliere = L.circle([parseFloat(vett_latitudine_rast[i]).toFixed(6),parseFloat(vett_longitudine_rast[i]).toFixed(6)], {
            color: 'blue',
            fillColor: '#85C1E9',
            fillOpacity: 0.5,
            radius: 50
        })
        vett_dist_min_circle.push(circle_rastrelliere)
    }
    //per ogni oggetto json delle rastrelliere, inserisco nel popup l'elenco delle bici presenti
    for (var i = 0; i < jsonObject_rastrelliere.length; i++) {
        marker_rastrelliere_vettore[i].on('click', onClick);
        marker_rastrelliere_vettore[i].bindPopup('<p> '+jsonObject_rastrelliere[i].nome+'<br>Rental point ID: '+jsonObject_rastrelliere[i].id +'<br> Parked bikes: <br> ' + jsonObject_rastrelliere[i].bicicletta + '</p>')
    }
    function onClick(e) {
        if (mostra_distanza_massima){
            mostra_distanza_massima = false //disabilito l'area
            circle_rastrelliere_max.remove()
        }else{ //mostro l'area
            mostra_distanza_massima = true
             //cerchio di 20km, rappresentate la distanza massima 
            circle_rastrelliere_max = L.circle([this.getLatLng().lat,this.getLatLng().lng], {
                color: 'green',
                fillColor: '#008000',
                fillOpacity: 0.3,
                radius: 20000
            }).addTo(map);
        } 
    }
    if($('.rental-point').is(":checked")){ 
        for (var i=0; i<marker_rastrelliere_vettore.length; i++){
            marker_rastrelliere_vettore[i].addTo(markerGroup_rental)
            vett_dist_min_circle[i].addTo(markerGroup_rental)
        }
    }else{
        markerGroup_rental.clearLayers();
        //controllo che non ci sia disegnato il cerchio verde della distanza massima, se disegnato lo rimuovo
        if (mostra_distanza_massima){
            mostra_distanza_massima = false //disabilito l'area
            circle_rastrelliere_max.remove()
        }
    }
}



/***********************************************************************
 ****** Costruizione e visualizzazione delle checkbox sulla mappa ******
 ***********************************************************************/

var command = L.control({position: 'topright'});
command.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'command');
    div.innerHTML = '<form class="form-check"><input class="check_all" type="checkbox" name="check_all" value="1" onchange="valueChanged()"/> All bike position\
    <input class="check_single" type="checkbox" name="check_single" value="2" onchange="valueChanged2()"/> Show bike route<br><br>\
    <input class="rental-point" type="checkbox" name="rental-point" value="3" onchange="showRentalPoint()"/> Rental points<br><br>\
    <input class="poi" type="checkbox" name="poi" value="4" onchange="showGeofences()"/> Geofences <br><br>\
    <input class="cluster" type="checkbox" name="cluster" value="6" onchange="showCluster()"/> Clusters\
    </form>'; 
    return div;
};

/************************************************
 ****** Gestione data inerente ai cluster *******
 ************************************************/

var data_iniziale = url.searchParams.get("data_iniziale");
$(function () {
    $("#datepicker2").datepicker({ 
            autoclose: true
    });    
});
if (data_iniziale!=null){
    $('.data_inserita2').val(data_iniziale) 

}
var data_finale = url.searchParams.get("data_fine");
$(function () {
    $("#datepicker3").datepicker({ 
            autoclose: true
    });    
});
if (data_finale!=null){
    $('.data_inserita3').val(data_finale) 
}

/***********************************************************************************
 ****** Costruizione e visualizzazione dello slider per la scelta dei clustr *******
 ***********************************************************************************/

$("#btn-cluster").click( function() {
    var data_inserita2 = $('#datepicker2').val();
    var data_inserita3 = $('#datepicker3').val();
    window.location = "https://norwegiangoat.ddns.net/bike_app/index/?ncluster="+parseInt($value.val())+"&bike_id=" + parseInt(bici_selezionata)+"&data="+data_selezionata+"&data_iniziale="+data_inserita2+"&data_fine="+data_inserita3;
});

/**********************************************************************
 ****** Gestione dei cluster NON FILTRATI ritornati dal backend *******
 **********************************************************************/

var cluster_layer = L.layerGroup().addTo(map);
var vettore_cluster = []
for(var i=0; i<json_clusters.length; i++){
    var cluster_point = L.circle([json_clusters[i].latitude,json_clusters[i].longitude], {
        color: json_clusters[i].color,
        fillColor: json_clusters[i].color,
        fillOpacity: 0.5,
        radius: 40
    });
    vettore_cluster.push(cluster_point)
}


function showCluster(){
    if($('.cluster').is(":checked")){ //o se nell url è settato il numero di cluster
        for (var i=0; i<vettore_cluster.length; i++){
            vettore_cluster[i].addTo(cluster_layer)
        }
    }else{
        cluster_layer.clearLayers();
    }
}

/************************************************************************
 ****** Gestione della visualizzazione geofence POI  e RESTRICTED *******
 ************************************************************************/
var geofence_poi_layer = L.layerGroup().addTo(map);
var poi = null

function showGeofences(){
    poi = L.geoJSON(json_ingressi_results, {
               style: function(feature) {
                    return feature.style
                }
    });

    if($('.poi').is(":checked")){ 
        poi.addTo(geofence_poi_layer);    
    }else{
        geofence_poi_layer.clearLayers()
    }
}
command.addTo(map);
