from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from bike_app.api.serializers import PrenotazioneSerializer_vera, NoleggioSerializer_vera, PrenotazioneSerializer_vera, RastrellieraSerializer_vera, GeofenceSerializer, BiciclettaSerializer, PrenotazioneSerializerTutte, NoleggioSerializerTutte, RastrellieraSerializerTutte
from bike_app.models import Geofence, Noleggio, Bicicletta, Rastrelliera, Prenotazione
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from django.conf import settings
from accounts.models import Account
import random
from datetime import datetime
from django.http import Http404
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework import permissions
from django.db import connection
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point


# DISTANZA TOLLERATA TRA LA BICICLETTA E LA RASTRELLIERA IN METRI (50m)
DISTANZA_MINIMA = 50

# DISTANZA MASSIMA TRA UNA BICICLETTA ED UNA RASTRELLIERA (20km)
DISTANZA_MASSIMA = 20000

# metodo che permette di visualizzare tutte le prenotazioni, qualora l'utente abbia fatto
# una richiesta inserendo il token.


@permission_classes([IsAuthenticated])
@api_view(['GET', 'POST', ])
# mostro le prenotazioni di un'utente solo
def show_prenotazione_filtrata(request):
    user_richiedente = request.user
    if request.method == 'GET':
        # non bisogna mostrare le prenotazioni più vecchie della data di oggi, mostro solo quelle future
        oggi = datetime.now().date()
        queryset = Prenotazione.objects.filter(
            user=user_richiedente, giornata_prenotata__gte=oggi).order_by('id')
        serializer = PrenotazioneSerializer_vera(queryset, many=True)
        return Response(serializer.data)
    if request.method == 'POST':
        prenotazione_json_data = JSONParser().parse(request)
        # DEVO GENERARE QUI IL CODICE DI SBLOCCO ED INVIARLO ALL'UTENTE DOPO CHE HA FATTO LA POST, come risposta
        # devo controllare che il codice sblocco non esista già? non è importante, ogni prenotazione genera un codice di sblocco
        numero_casuale = random.randint(1, 999999)
        prenotazione_json_data['codice_sblocco'] = numero_casuale

        # controllare che la bici non sia già prenotata. Controllo fatto per giornata non per ore all'interno di essa
        # cioè: controllo che per la giornata prenotata e PER QUELLA BICI non ci siano già prenotazioni
        giornata_prenot = prenotazione_json_data['giornata_prenotata']  # [:10]
        bicicletta_id = prenotazione_json_data['bicicletta']

        # calcolo la giornata di oggi e vedo se la giornata prenotata è corretta
        oggi = datetime.now()
        # valore della giornata prenotata formattato
        check_date = datetime.strptime(giornata_prenot, "%Y-%m-%d")

        # devo controllare se la bici è in giro per le strade, ovvero se qualcuno la sta adoperando. Se si, non devo permettere di prenotarla per oggi
        # controllo se la bici è parcheggiata ==> per prenotarla oggi la bici deve essere in una rastrelliera cioè non deve essere in uso
        bici_parcheggiata = Rastrelliera.objects.filter(
            bicicletta=bicicletta_id).exists()  # è TRUE se è parcheggiata
        #print("La bici è parcheggiata in una rastrelliera? ",bici_parcheggiata)

        # controllo che la data inserita non sia precedente alla data di oggi e che la bici sia in una rastrelliera
        if not(check_date.date() < oggi.date()) and bici_parcheggiata:
            data_formattata_prenotazione = datetime.strptime(
                giornata_prenot, '%Y-%m-%d').date()
            prenotazione_json_data['giornata_prenotata'] = str(
                data_formattata_prenotazione)
            query_check = Prenotazione.objects.filter(
                giornata_prenotata=data_formattata_prenotazione).filter(bicicletta=bicicletta_id).exists()
            if (query_check == False):  # controllo se la data prenotata esiste già nella tabella Prenotazione
                utente = request.user
                if request.method == 'POST':
                    prenotazione_json_data['user'] = Account.objects.get(
                        email=utente).id
                    serializer_prenotazione = PrenotazioneSerializer_vera(
                        data=prenotazione_json_data)
                    if serializer_prenotazione.is_valid():
                        serializer_prenotazione.save()
                        return Response(prenotazione_json_data, status=status.HTTP_201_CREATED)
                    print("IL SERIALIZER NON è VALIDO")
                    return Response(serializer_prenotazione.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response({"Response": "Error. There is an other reservation for this bike this day."}, status=status.HTTP_400_BAD_REQUEST)
        elif (bici_parcheggiata == False):
            return Response({"Response": "Bike is running. Someone is using it."})
        else:
            return Response({"Response": "Error. Incorrect day inserted."})


# recupero la posizione dell'utente (contenuta all'interno della tabella noleggio del backend)
# solo gli utenti autenticati possono inviare la posizione
@permission_classes([IsAuthenticated])
# bisogna passare l'id della bici nell url, e la posizione della bici nel body e IL CODICE DI SBLOCCO
@api_view(['POST', ])
def inserisci_posizione_in_noleggio(request, pk):
    # Prima di iniziare il noleggio devo controllare che la bici sia in una rastrelliera
    queryset_bici = Rastrelliera.objects.filter(bicicletta=pk)
    noleggio_json_data = JSONParser().parse(request)
    if len(queryset_bici) != 0:  # se la query contiene elementi (ha trovato la bici nella rastrelliera)
        # CONTROLLO CHE LA GIORNATA IN CORSO SIA EFFETTIVAMENTE QUELLA CHE L'UTENTE HA PRENOTATO
        # IN CASO AFFERMATIVO GLI PERMETTO DI INIZIARE IL NOLEGGIO. ALTRIMENTI BLOCCO TUTTO.
        # TALE CONTROLLO VA FATTO SOLO UNA VOLTA, QUANDO LA BICI SI TROVA ANCORA NELLA RASTRELLIERA, OVVERO ORA
        # cerco la prenotazione dell'utente per oggi e per questa bici
        oggi = datetime.now()
        # controllo anche che il codice di sblocco inserito sia esattamente quello della prenotazione a lui associata
        # se questo valore è FALSE l'utente può comunque avviare il noleggio (perchè la bici non è prenotata per ogg)
        bici_prenotata = Prenotazione.objects.filter(
            giornata_prenotata=oggi.date()).filter(bicicletta=pk).exists()
        query_check = False
        if bici_prenotata:
            query_check = Prenotazione.objects.filter(giornata_prenotata=oggi.date()).filter(
                bicicletta=pk).filter(codice_sblocco=noleggio_json_data['codice_sblocco']).exists()

        # print(query_check) #è TRUE se esiste una prenotazione per la data di oggi per quella bici, verificando se il codice di sblocco inserito dall'utente è corretto

        # trovo l'id dell'utente che sta provando ad avviare il noleggio per capire se quella prenotazione di oggi (nel caso esista) sia fatta da lui
        email_utente = request.user
        id_utente = Account.objects.get(email=email_utente).id
        query_check2 = False
        if (query_check):  # se esiste una prenotazione per quella bici con quel codice sblocco e per la giornata di oggi
            # controllo se la prenotazione è dell'utente che sta provando ad avviare il noleggio
            # questa variabile è TRUE se la prenotazione è associata all'utente richiedente il servizio di avvio noleggio
            query_check2 = Prenotazione.objects.filter(user=id_utente).exists()
        # se la bici non è prenotata puoi avviare il noleggio, quindi uso una variabile booleana bici_prenotata
        # controllo che la bici sia stata prenotata da quella persona OPPURE controllo che non sia prenotata (se non è prenotata il noleggio può comunque partire).
        # se la prenotazione esiste ( ed è di quell utente) oppure se non esiste alcuna prenotazione, allora vado avanti a fare la POST
        if (query_check2 or bici_prenotata == False):
            # rimuovo la bici dalla rastrelliera
            # Rastrelliera.bicicletta.through ====> dovrebbe essere la tabella intermedia tra rastrelliera e bicicletta, creata dalla manytomany
            query_rimozione = Rastrelliera.bicicletta.through.objects.filter(
                bicicletta=pk).delete()

            # ---------- INIZIO DELL'INSERIMENTO DELLA POSIZIONE NEL DATABASE
            email_ricevuta = request.user
            if request.method == 'POST':
                # recupero l'id grazie alla mail di chi mi sta facendo la richiesta
                # metto l'id nel json che passo al database nel post
                noleggio_json_data['user'] = Account.objects.get(
                    email=email_ricevuta).id
                noleggio_json_data['bicicletta'] = pk
                serializer_noleggio = NoleggioSerializer_vera(
                    data=noleggio_json_data)
                if serializer_noleggio.is_valid():
                    serializer_noleggio.save()
                    # return Response(noleggio_json_data, status=status.HTTP_201_CREATED)
                    return Response({"Response": "Rental started with success."})
                print("IL SERIALIZER NON è VALIDO")
                return Response(serializer_noleggio.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"Response": "Maybe the inserted code is incorrect or you have not booked this bike today."})
    elif len(queryset_bici) == 0:  # se la bici non c'è nella rastrelliera è molto probabile che la stia usando il tipo che ha richiesto il noleggio
        # controllo che l'utente non stia provando a noleggiare una bici che non sia sua
        # ovvero: per continuare il noleggio (dopo averlo avviato la prima volta) l'utente potrà utilizzare la bici
        # solo se è l'utente stesso che ha avviato il noleggio

        # ----- ottengo l'id dell utente richiedente
        email_user = request.user
        id_utente = Account.objects.get(email=email_user).id
        # controllo se l'ultimo noleggio inserito PER QUELLA BICI sia effettuato dell'utente richiedente. Qualora altri provassero a continuare il
        # suo noleggio, verrebbero bloccati
        utente_corretto = Noleggio.objects.filter(
            user=id_utente).filter(bicicletta=pk).exists()
        if utente_corretto:
            # prendo l'ultimo noleggio inserito per quella bici e recupero chi lo ha fatto
            noleggio_utente = Noleggio.objects.filter(
                bicicletta=pk).order_by('-id').values('user')[0]
            if noleggio_utente['user'] == id_utente:
                # RIFACCIO L'INSERIMENTO DELLA NUOVA POSIZIONE NEL DATABASE
                if request.method == 'POST':
                    # recupero l'id grazie alla mail di chi mi sta facendo la richiesta
                    # metto l'id nel json che passo al database nel post
                    noleggio_json_data['user'] = id_utente
                    noleggio_json_data['bicicletta'] = pk
                    serializer_noleggio = NoleggioSerializer_vera(
                        data=noleggio_json_data)
                    if serializer_noleggio.is_valid():
                        serializer_noleggio.save()

                        # dopo aver salvato la posizione nel database, controllo la distanza dalla rastrelliera piu vicina

                        # PER PRIMA COSA devo controllare che la bicicletta sia all'interno della distanza massima dalla rastrelliera più vicina (devo quindi trovare
                        # la rastrelliera più vicina)
                        cursor = connection.cursor()
                        # recupero l'ultima posizione inviata
                        # ultima posizione inviata
                        query_ultimo_noleggio_inviato = "SELECT location FROM bike_app_noleggio WHERE bicicletta_id='%s' ORDER BY ID DESC LIMIT 1" % pk
                        cursor.execute(query_ultimo_noleggio_inviato)
                        last_posizione_bici = cursor.fetchall()

                        # trovo quindi la rastrelliera più vicina
                        posizione_bici = noleggio_json_data['location']
                        query_posizione = "SELECT A.id, MIN(ST_DISTANCE(A.location, '%s')) AS distance FROM bike_app_rastrelliera AS A GROUP BY A.id ORDER BY distance ASC limit 1" % last_posizione_bici[
                            0]
                        cursor.execute(query_posizione)
                        rows = cursor.fetchall()
                        if(rows[0][1] > DISTANZA_MASSIMA):
                            debug_print_distance = "DISTANZA DI {1} METRI DALLA RASTRELLIERA PIù VICINA CON ID {0}.".format(
                                rows[0][0], rows[0][1])
                            print(debug_print_distance)
                            return Response({"Response": "WARNING: Bike is too far away from the nearest rental point."})
                        else:
                            geofence_data = check_geofence(posizione_bici)
                            if geofence_data:
                                message = check_geofence(posizione_bici)[0]
                                geofence_type = check_geofence(
                                    posizione_bici)[1]
                                return Response({"Response": "Rental position update with success.", "Message": message, "Type": geofence_type})
                            else:
                                return Response({"Response": "Rental position update with success."})
                    print("IL SERIALIZER NON è VALIDO")
                    return Response(serializer_noleggio.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response({"Response": "Error. This bike is not your."})
        return Response({"Response": "Error. This bike is booked."})
    else:
        return Response({"Response": "Bike not found in rental point. Maybe somebody is using it."})


def check_geofence(posizione_bici):
    longitude = str(posizione_bici["coordinates"][0])
    latitude = str(posizione_bici["coordinates"][1])
    cursor = connection.cursor()
    is_in_geofence = "SELECT messaggio, tipologia FROM(SELECT G.tipologia, G.messaggio, ST_Intersects(geofence_area::geometry, ST_GeomFromText('POINT({} {})', 4326)) FROM bike_app_geofence AS G) AS check_g WHERE st_intersects = true".format(
        longitude, latitude)
    cursor.execute(is_in_geofence)
    response = cursor.fetchall()
    print("response ", response)
    if response:
        # Returns the geofence data
        return response[0]
    else:
        # If the point isn't in a geofence it returns the empty string
        return ""


# api per terminare il noleggio, la bici va aggiunta ad una rastrelliera
# questa query utilizza la distanza, verifica che la rastrelliera sia distante almeno tot metri (costante dichiarata a inizio codice)
@permission_classes([IsAuthenticated])
@api_view(['POST', ])
def termina_noleggio(request, pk):  # nel body andrà passata la posizione dell'utente
    # LA BICI DEVE ESSERE RI-AGGIUNTA AD UNA RASTRELLIERA
    # se la bici non è in nessuna rastrelliera allora IL NOLEGGIO è IN CORSO
    rastrelliera_json_data = JSONParser().parse(request)
    if request.method == 'POST':
        # l'utente mi passa la posizione della bici
        # cerco la rastrelliera con quella posizione e ci metto dentro la bicicletta

        rastrelliera_json_data['bicicletta'] = pk

        # trovo la posizione dell ultima posizione-noleggio inviata dall utente
        cursor = connection.cursor()
        # ultima posizione inviata
        query_ultimo_noleggio_inviato = "SELECT location FROM bike_app_noleggio WHERE bicicletta_id='%s' ORDER BY ID DESC LIMIT 1" % pk
        cursor.execute(query_ultimo_noleggio_inviato)
        risultato_pos_utente = cursor.fetchall()

        # ottengo tutte le distanze dalla bicicletta a tutte le rastrelliere
        query_posizione = "SELECT A.id, ST_DISTANCE(A.location, '%s') AS distance FROM bike_app_rastrelliera AS A ORDER BY distance ASC limit 1" % risultato_pos_utente[
            0]
        cursor.execute(query_posizione)
        rows = cursor.fetchall()
        rastr_id = rows[0][0]  # id rastrelliera proveniente dalla query

        # distanza calcolata dalla query
        distanza_rastrelliera_posizione = rows[0][1]
        # controllo che l'id della bici non sia presente nella rastrellieara
        id_presente = Rastrelliera.objects.filter(bicicletta=pk)

        posizione_rastrelliera_query = Rastrelliera.objects.get(id=rastr_id)
        bici_istance = Bicicletta.objects.get(id=pk)
        print("Distanza bicicletta dalla rastrelliera: ",
              distanza_rastrelliera_posizione)
        # GESTISCO LA RESPONSE SOTTO SULLA BASE DELLA COSTANTE DELLA DISTANZA MINIMA
        if distanza_rastrelliera_posizione > DISTANZA_MINIMA:
            return Response({"Response": "The bike is far away from rental point"})
        else:  # --------> IN QUESTO CASO la bici non viene parcheggiata nella rastrelliera, L'UTENTE DOVRà RICHIAMARE L'API DELL NOLEGGIO per poi richiamare quella di TERMINA noleggio
            if len(id_presente) == 0:  # se non c'è, inserisco l'id
                valori_inseriti = Rastrelliera.bicicletta.through.objects.create(
                    bicicletta=bici_istance, rastrelliera=posizione_rastrelliera_query)
                return Response({"Response": "Bike parked in rental point with success."})
            return Response({"Response": "The bike is parked in this rental point yet"})


# api DELETE di una prenotazione effettuata da un particolare utente
@permission_classes([IsAuthenticated])
@api_view(['DELETE', ])
def delete_prenotazione(request, pk):
    #id_prenotazione_da_rimuovere = request.data['id']
    try:
        prenotazione_cercata = Prenotazione.objects.get(id=pk)
    except:
        return Response("This reservetion does not exist.")
    if request.method == 'DELETE':
        # permetto la rimozione della prenotazione solo se è l'utente stesso ad averla creata
        if request.user == prenotazione_cercata.user:
            prenotazione_cercata.delete()
            return Response({"Response": "Reservation has been deleted."})
        return Response({"Response": "Error. You can't delete this reservation. This is not yours."})
    return Response({"Response": "Error. Reservation has not been deleted."})


# api che restituisce in quale rastrelliera si trova la bicicletta.
# nell url bisogna passare l'id della bicicletta passata
@permission_classes([IsAuthenticated])
@api_view(['GET', ])
def cerca_bicicletta(request, pk):
    try:
        if request.method == 'GET':
            rastrelliere = Rastrelliera.objects.filter(bicicletta=pk)
            serializer = RastrellieraSerializer_vera(rastrelliere, many=True)
            return Response(serializer.data[0]['location'])
    except:
        # la rastrelliera potrebbe non esistere o la bici potrebbe essere in utilizzo
        return Response({"Response": "Error. No rental point available."})


#############################################################################################################################
################################### classi che mostrano tutti gli oggetti, sono tutte GET ###################################
#############################################################################################################################
# serializers importati
@permission_classes([IsAuthenticated])
class GeofenceViewSet(viewsets.ModelViewSet):
    queryset = Geofence.objects.all()
    serializer_class = GeofenceSerializer
    permission_classes = [permissions.IsAuthenticated]


@permission_classes([IsAuthenticated])
class BiciclettaViewSet(viewsets.ModelViewSet):
    queryset = Bicicletta.objects.all().order_by('id')
    serializer_class = BiciclettaSerializer
    permission_classes = [permissions.IsAuthenticated]  # IsOwnerOrReadOnly

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


@permission_classes([IsAuthenticated])
class PrenotazioneViewSet(viewsets.ModelViewSet):
    queryset = Prenotazione.objects.all().order_by('id')
    serializer_class = PrenotazioneSerializerTutte
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


@permission_classes([IsAuthenticated])
class NoleggioViewSet(viewsets.ModelViewSet):
    queryset = Noleggio.objects.all().order_by('id')
    serializer_class = NoleggioSerializerTutte
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(user_noleggio=self.request.user)


@permission_classes([IsAuthenticated])
class RastrellieraViewSet(viewsets.ModelViewSet):
    queryset = Rastrelliera.objects.all().order_by('id')
    serializer_class = RastrellieraSerializerTutte
    permission_classes = [permissions.IsAuthenticated]
