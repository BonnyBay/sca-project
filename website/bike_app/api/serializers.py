from rest_framework import status
from rest_framework.response import Response
from rest_framework import serializers
from django.contrib.auth.models import User
from django.conf import settings

from accounts.models import Account
from bike_app.models import Prenotazione, Noleggio, Rastrelliera, Geofence, Bicicletta


class User_serializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['username']


class PrenotazioneSerializer_vera(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Prenotazione
        fields = ['id', 'user', 'bicicletta', 'codice_sblocco',
                  'timestamp', 'giornata_prenotata']


class NoleggioSerializer_vera(serializers.ModelSerializer):
    class Meta:
        model = Noleggio
        fields = ['id', 'user', 'bicicletta', 'location']


class PrenotazioneSerializer_vera(serializers.ModelSerializer):
    class Meta:
        model = Prenotazione
        fields = ['id', 'user', 'bicicletta', 'codice_sblocco',
                  'giornata_prenotata', 'timestamp']


class RastrellieraSerializer_vera(serializers.ModelSerializer):
    class Meta:
        model = Rastrelliera
        fields = ["id", "bicicletta", "location", "nome"]


################################################################################################
#################################   Serializer per mostrare tutto   ############################
################################################################################################

class GeofenceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        ordering = ['-id']  # uso il meno perchè sono in ordine discendente
        model = Geofence
        fields = ['id', 'geofence_area', 'messaggio', 'tipologia']


class BiciclettaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bicicletta
        fields = ['id']

# Prenotazione: è la tabella che relazione utente con bicicletta => un utente prenota una bicicletta


class PrenotazioneSerializerTutte(serializers.HyperlinkedModelSerializer):
    bicicletta = BiciclettaSerializer()
    user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Prenotazione
        fields = ['id', 'user', 'bicicletta', 'codice_sblocco',
                  'timestamp', 'giornata_prenotata']


class NoleggioSerializerTutte(serializers.HyperlinkedModelSerializer):
    bicicletta = serializers.PrimaryKeyRelatedField(read_only=True)

    user_noleggio = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Noleggio
        fields = ['id', 'user_noleggio', 'bicicletta', 'location', 'timestamp']


class RastrellieraSerializerTutte(serializers.HyperlinkedModelSerializer):
    bicicletta = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Rastrelliera
        fields = ["id", "bicicletta", "location", "nome"]
