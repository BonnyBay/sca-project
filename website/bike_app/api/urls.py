from django.urls import path
from rest_framework import routers
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from bike_app.api.views import(
    show_prenotazione_filtrata,
    inserisci_posizione_in_noleggio,
    delete_prenotazione,
    cerca_bicicletta,
    termina_noleggio,

)


app_name = "bike_app"


urlpatterns = [
    path('prenotazione_utente/', views.show_prenotazione_filtrata,
         name='prenotazione_utente'),
    path('noleggio_utente/<str:pk>/',
         views.inserisci_posizione_in_noleggio, name='noleggio_utente'),
    path('cancella_prenotazione/<str:pk>/',
         views.delete_prenotazione, name='cancella_prenotazione'),
    path('cerca_bicicletta/<str:pk>/',
         views.cerca_bicicletta, name='cerca_bicicletta'),
    path('termina_noleggio/<str:pk>/',
         views.termina_noleggio, name='termina_noleggio'),


]
