from django.contrib import admin, messages
from django.urls import path
from django.shortcuts import render
from django.utils.html import json_script
from .models import Geofence, Rastrelliera, Noleggio, Bicicletta, Prenotazione
from django.contrib.auth.models import User
from leaflet.admin import LeafletGeoAdmin
from django import forms
from .api.serializers import GeofenceSerializer, RastrellieraSerializer_vera
import json
from django.db import connection

admin.site.register(Noleggio, LeafletGeoAdmin)
admin.site.register(Bicicletta)
admin.site.register(Prenotazione)

class JsonImportForm(forms.Form):
    json_upload = forms.FileField()

class GeofenceTypeForm(forms.Form):
    geofence_type = forms.ChoiceField(widget=forms.RadioSelect, choices=[("POI","POI"), ("RESTRICTED","RESTRICTED")], initial="POI")

def check_object(text_representation, table_name, col_name):
    cursor = connection.cursor()
    object_exists = "SELECT * FROM " + table_name + " WHERE " + col_name + " = ST_GeomFromText('" + text_representation + "')"
    cursor.execute(object_exists)
    response = cursor.fetchall()
    print("Upload data from JSON, if object exists: ",response)
    return response

@admin.register(Geofence)
class GeofenceAdmin(LeafletGeoAdmin):
    def get_urls(self):
        urls = super().get_urls()
        upload_json_url = [path('upload_json/', self.admin_site.admin_view(self.upload_json)),]
        return upload_json_url + urls
    
    def upload_json(self, request):
        if request.method == "POST":
            json_data = request.FILES["json_upload"]
            json_data = json.loads(json_data.read())
            try:
                for json_object in json_data['features']:
                    #Construct object
                    message = json_object['properties']['name']
                    tipologia = GeofenceTypeForm(request.POST)
                    if tipologia.is_valid():
                        tipologia = tipologia.cleaned_data['geofence_type'] #Getting data from bound form
                    geofence_area = "SRID=4326;POLYGON(("
                    for coordinate in json_object['geometry']['coordinates'][0]:
                        lon = coordinate[0]
                        lat = coordinate[1]
                        geofence_area += str(lon) + " " + str(lat) + ","
                    geofence_area = geofence_area[:-1] #Remove last comma
                    geofence_area += "))"
                    my_obj = {"messaggio" : message,
                    "geofence_area" : geofence_area,
                    "tipologia" : tipologia}
                    print(my_obj)
                    #Save object if valid and if is not present in db
                    is_object_existent = check_object(geofence_area, "bike_app_geofence", "geofence_area")
                    if is_object_existent:
                        messages.warning(request, "Geofence already exists")
                    else:
                        serializer_obj = GeofenceSerializer(data= my_obj)
                        if serializer_obj.is_valid():
                            messages.success(request, "Geofence added successfully")
                            serializer_obj.save()
                        else:
                            messages.warning(request, serializer_obj.errors)
            except:
                #Return invalid to frontend
                messages.warning(request, "File format not vaild")
        form = JsonImportForm()
        geofence_type = GeofenceTypeForm()
        data = {"form" : form, "geofence_type" : geofence_type}
        return render(request, 'admin/upload_json.html', data)

@admin.register(Rastrelliera)
class RastrellieraAdmin(LeafletGeoAdmin):
    def get_urls(self):
        urls = super().get_urls()
        upload_json_url = [path('upload_json/', self.admin_site.admin_view(self.upload_json)),]
        return upload_json_url + urls
    
    def upload_json(self, request):
        if request.method == "POST":
            json_data = request.FILES["json_upload"]
            json_data = json.loads(json_data.read())
            try:
                for json_object in json_data['features']:
                    #Construct object
                    nome = json_object['properties']['Nome']
                    location = "SRID=4326;POINT(" + str(json_object['geometry']\
                        ['coordinates'][0]) + ' ' + str(json_object['geometry']\
                        ['coordinates'][1]) + ')'
                    my_obj = {"nome" : nome,
                    "location" : location,
                    "bicicletta" : []}
                    print(my_obj)
                    #Save object if valid
                    #Save object if valid and if is not present in db
                    is_object_existent = check_object(location, "bike_app_rastrelliera", "location")
                    if is_object_existent:
                        messages.warning(request, "Rastrelliera already exists")
                    else:
                        serializer_obj = RastrellieraSerializer_vera(data= my_obj)
                        if serializer_obj.is_valid():
                            messages.success(request, "Rastrelliera added successfully")
                            serializer_obj.save()
                        else:
                            messages.warning(request, serializer_obj.errors)
            except:
                #Return invalid to frontend
                messages.warning(request, "File format not vaild")
        form = JsonImportForm()
        data = {"form" : form}
        return render(request, 'admin/upload_json.html', data)