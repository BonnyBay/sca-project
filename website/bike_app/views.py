from django.http import HttpResponse
from bike_app.models import Noleggio, Bicicletta, Rastrelliera, Geofence
from django.db import connection
from django.shortcuts import render
# importing loading from django template
from django.template import loader
from rest_framework.response import Response
from rest_framework import status
import json
from typing import List
import datetime
from bike_app.api.serializers import RastrellieraSerializerTutte, NoleggioSerializerTutte, BiciclettaSerializer, GeofenceSerializer
from django.db.models import Max
from django.contrib.gis.db.models.functions import AsGeoJSON
from django.core.serializers import serialize
from datetime import datetime

from sklearn.cluster import KMeans
import numpy as np


VETTORE_COLORI = ["#0000ff", "#ff0000", "#4E77EE", "#B61938", "#FD89FB", "#6002EE", "#61d800",
                  "#ff8d00", "#c7006e", "#0000e4"]  # massimo 10 colori, verranno assegnati ai vari cluster


def index(request):
    template = loader.get_template('index.html')  # getting our template

    # mostro rastrelliere sulla mappa con le bici presenti
    query_rastrelliere_bici = Rastrelliera.objects.all().order_by('id')
    serializer_class_rastr = RastrellieraSerializerTutte(
        query_rastrelliere_bici, many=True)

    # ritorno tutte le biciclette e le mostro nel menu a tendina
    query_bici = Bicicletta.objects.all().order_by('id')
    serializer_class_bici = BiciclettaSerializer(query_bici, many=True)
    json_data_bici = json.dumps(serializer_class_bici.data, indent=4)
    # INIZIO parte noleggi
    cursor = connection.cursor()
    # ultima posizione inviata
    query_ultimo_noleggio_inviato = "SELECT ST_asGeoJson(location),bicicletta_id FROM bike_app_noleggio WHERE timestamp = any (SELECT MAX(timestamp) FROM bike_app_noleggio GROUP BY bike_app_noleggio.bicicletta_id)  "
    cursor.execute(query_ultimo_noleggio_inviato)
    risultato_pos_utente = cursor.fetchall()
    # vettore che conterrà gli id delle bici associati ai noleggi di data_noleggi_pos
    data_noleggi_id = []
    data_noleggi_lat = []
    data_noleggi_long = []
    data_last_user = []
    for noleggio in risultato_pos_utente:
        data_noleggi_id.append(noleggio[1])
        ultima_posizione = Noleggio.objects.filter(
            bicicletta=noleggio[1]).order_by('-id')[0]
        longitude = ultima_posizione.location.x
        latitude = ultima_posizione.location.y
        data_noleggi_lat.append(str(latitude))
        data_noleggi_long.append(str(longitude))
    # FINE parte noleggi

    # aggiungo le rastrelliere presenti e i noleggi al json da restituire
    json_data_rastr = json.dumps(
        serializer_class_rastr.data, default=lambda o: o.__dict__, indent=4)

    # QUERY CHE DATA UN ID DELLA BICI TI RITORNA TUTTO LO STORICO
    id_bici = request.GET.get('bike_id', '')

    data_inserita = request.GET.get('data', '')

    last_single_bike_position = ""
    try:
        query_storico_single_bici = Noleggio.objects.filter(bicicletta=id_bici)
        if(data_inserita != ''):  # se la data non fosse settata, mostrerei tutti i percorsi di quella singola bici fatti da sempre
            check_date = datetime.strptime(data_inserita, "%Y-%m-%d")
            query_storico_single_bici = Noleggio.objects.filter(
                bicicletta=id_bici).filter(timestamp__startswith=check_date.date())
        serializer_class_noleggi_single = NoleggioSerializerTutte(
            query_storico_single_bici, many=True)
        last_single_bike_position = json.dumps(
            serializer_class_noleggi_single.data, indent=4)
        #print(json.dumps(serializer_class_noleggi_single.data, indent=4))
    except:
        print("errore, id vuoto")

    ###############################################################################################################################################
    ########################## * CLUSTERING DI TUTTE LE POSIZIONI DELLE BICICLETTE (senza e con filtri applicati) * ################################
    ###############################################################################################################################################
    n_cluster = request.GET.get('ncluster', '')
    data_iniziale = request.GET.get('data_iniziale', '')
    data_finale = request.GET.get('data_fine', '')

    vettore_coordinate_bici = []
    json_cluster = []
    json_cluster_output = []
    if n_cluster != '':  # se l'utente ha settato un numero di cluster da visualizzare
        if data_iniziale == '' or data_finale == '':
            query = Noleggio.objects.all()
        else:
            # CLUSTERING DI TUTTE LE POSIZIONI DELLE BICICLETTE CON FILTRI APPLICATI
            query = Noleggio.objects.filter(
                timestamp__range=[data_iniziale, data_finale])
        # il numero di cluster deve essere minore uguale del numero di noleggi presenti/considerati
        if int(n_cluster) <= len(query):
            if len(query):  # deve ritornarmi almeno un elemento, se è 0 allora è false
                for location_bici in query:
                    coordinate_bici = []
                    longitude_bici = location_bici.location.x
                    latitude_bici = location_bici.location.y
                    coordinate_bici.append(latitude_bici)
                    coordinate_bici.append(longitude_bici)
                    vettore_coordinate_bici.append(coordinate_bici)
                # -----------------------------------------------------------
                # ---------------- INIZIO ALGORITMO K MEANS ----------------
                # -----------------------------------------------------------
                coordinates = np.array(vettore_coordinate_bici)
                kmeans = KMeans(n_clusters=int(n_cluster))
                kmeans.fit(coordinates)

                # ottengo i centroidi dei vari cluster
                centroidi = kmeans.cluster_centers_
                # print(centroidi)

                # ottengo i clusters
                clusters = kmeans.fit_predict(coordinates)
                # print(clusters)
                # ----------------------------------------------------
                # ---------------- FINE ALGORITMO K MEANS -----------
                # ----------------------------------------------------

                for item in range(0, len(clusters)):
                    json_cluster.append({
                        "latitude": vettore_coordinate_bici[item][0],
                        "longitude": vettore_coordinate_bici[item][1],
                        "color": VETTORE_COLORI[clusters[item]]
                    })

                json_cluster_output = json.dumps(json_cluster, indent=4)
        else:
            print("errore, il numero di cluster è maggiore del numero di noleggi")

    # CONTO IL NUMERO DI ATTIVAZIONE DI OGNI GEOFENCE

    cursor = connection.cursor()
    query_contatore = "SELECT count(*), id_geof, st_intersects, geofence_area, tipologia FROM (SELECT ST_Intersects(geofence_area::geometry, location::geometry), bike_app_geofence.id as id_geof, geofence_area, tipologia FROM bike_app_geofence, bike_app_noleggio) as ingressi GROUP BY geofence_area, id_geof, tipologia, st_intersects"
    cursor.execute(query_contatore)
    results_contatore = cursor.fetchall()
    json_geofence_attivati = []
    max_attivazioni = 0

    for ingresso0 in results_contatore:
        ingresso_attivazioni = ingresso0[0]
        geofence_attivato0 = ingresso0[2]

        # calcolo il massimo solo per i geofence attivati (st intersects = true)
        if geofence_attivato0:

            if max_attivazioni < ingresso_attivazioni:
                max_attivazioni = ingresso_attivazioni

    for ingresso in results_contatore:
        # recupero l'oggetto goefence con le coordiante serializzate nel modo corretto e lo aggiungo al json risultato che passerò al frontend
        query_geofence3 = Geofence.objects.filter(id=ingresso[1])
        geofence_serializer3 = GeofenceSerializer(query_geofence3, many=True)
        geof_area = geofence_serializer3.data[0]['geofence_area']
        tipologia_geof = ingresso[4]
        geofence_attivato = ingresso[2]

        colore_geofence = ""
        if tipologia_geof == "POI":
            colore_geofence = VETTORE_COLORI[0]
        else:  # RESTRICTED
            colore_geofence = VETTORE_COLORI[1]
        fill_opacity_calcolata = 0

        if geofence_attivato:
            fill_opacity_calcolata = ingresso[0]/max_attivazioni

        json_geofence_attivati.append({
            "type": "Feature",
            "properties": {"tipologia": tipologia_geof, "numero_attivazioni": ingresso[0], "id_geofence": ingresso[1]},
            "geometry": geof_area,
            "style": {
                "color": colore_geofence,
                "fillColor": colore_geofence,
                "fillOpacity": fill_opacity_calcolata,
                "opacity": 1,
            }
        })

    json_geofence_attivati = {
        "type": "FeatureCollection",
        "features": json_geofence_attivati}
    json_geofence_attivati_output = json.dumps(
        json_geofence_attivati, indent=4)

    # 'json_vietati_results':json_vietati_geofence, 'json_poi_results':json_poi_geofence,
    return render(request, 'index.html', {'json_data_result_rastrelliere': json_data_rastr, 'single_pos_bike': last_single_bike_position, 'data_result_noleggi_lat': data_noleggi_lat, 'data_result_noleggi_long': data_noleggi_long, 'data_result_id_bike_noleggi': data_noleggi_id, 'biciclette_json': json_data_bici, 'json_cluster_objects': json_cluster_output, 'json_ingressi_results': json_geofence_attivati_output})
