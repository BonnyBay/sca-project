from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls import url
import debug_toolbar
schema_view = get_swagger_view(title='User API')
urlpatterns = [
    path('bike_app/', include('bike_app.urls')),
    path('admin/', admin.site.urls),
    path('bike_app/api/accounts/', include('accounts.api.urls', 'accounts_api')),
    path('bike_app/api/', include('bike_app.api.urls', 'bike_app_api')),
    url('swagger/', schema_view),
    path('__debug__/', include(debug_toolbar.urls)),


]
