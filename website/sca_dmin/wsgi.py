"""
WSGI config for sca_dmin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""

import os, sys

from django.core.wsgi import get_wsgi_application
#sys.path.append('/home/git/sca-project/website')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sca_dmin.settings')

application = get_wsgi_application()
